cmake_minimum_required( VERSION 3.6 )

project( printFigure )
add_executable( ${PROJECT_NAME} "" )

target_include_directories( ${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_SOURCE_DIR} )
target_sources( ${PROJECT_NAME}
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/src/main.cpp
)

target_link_libraries( ${PROJECT_NAME} figures )
target_link_libraries( ${PROJECT_NAME} tools )

install( TARGETS ${PROJECT_NAME} DESTINATION bin )
