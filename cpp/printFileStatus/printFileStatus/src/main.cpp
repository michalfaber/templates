#include <iostream>
#include <filesystem>

namespace fs = std::filesystem;

void printPathStatus( const fs::path& aPath )
{
    std::cout << aPath;
    fs::file_status fStatus = fs::status( aPath );
    
    if ( fs::is_regular_file(   fStatus ) ) std::cout << " is a regular file\n";
    if ( fs::is_directory(      fStatus ) ) std::cout << " is a directory\n";
    if ( fs::is_block_file(     fStatus ) ) std::cout << " is a block device\n";
    if ( fs::is_character_file( fStatus ) ) std::cout << " is a character device\n";
    if ( fs::is_fifo(           fStatus ) ) std::cout << " is a named IPC pipe\n";
    if ( fs::is_socket(         fStatus ) ) std::cout << " is a named IPC socket\n";
    if ( fs::is_symlink(        fStatus ) ) std::cout << " is a symlink\n";
    if ( !fs::exists(           fStatus ) ) std::cout << " does not exist\n";
}

int main( int argc, char** argv )
{
    for ( int pathNumber = 0; pathNumber < argc; ++pathNumber )
    {
        printPathStatus( argv[pathNumber] );
        std::cout << std::endl;
    }

    return 0;
}
