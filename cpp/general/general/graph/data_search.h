#pragma once
#include "graph/solution.h"

namespace graph
{

struct DataSearch
{
    DataSearch( );
    void addSolution( const Solution& aSolution );

    std::string           mIdNodeToStart = "";
    std::string           mIdNodeToFind  = "";
    std::vector<Solution> mSolutions {};
    Solution              mSolutionBestOne;
};

} // namespace graph
