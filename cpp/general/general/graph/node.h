#pragma once
#include <iostream> // todo: remove
#include <memory>
#include <vector>

#include "graph/connection_half.h"

namespace graph
{

struct Node
{
    static std::unique_ptr<Node> Create( const std::string& id );

    Node( const std::string& id );
    ~Node( );
    const std::string& getId( ) const;
    void               addConnection( const ConnectionHalf& aConnection );

    const std::vector<ConnectionHalf>& getConnections( ) const;

private:
    std::string                 mId;
    std::vector<ConnectionHalf> mConnections;
};

} // namespace graph
