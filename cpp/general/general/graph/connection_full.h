#pragma once
#include "graph/connection_half.h"

namespace graph
{

struct ConnectionFull : public ConnectionHalf
{
    ConnectionFull( const std::string& aIdNodeStart,
                    const std::string& aIdDestination,
                    const uint32_t     aValue );
    const std::string& getIdNodeStart( );

private:
    std::string mIdNodeStart;
};

} // namespace graph
