#pragma once
#include <unordered_set>

#include "graph/comparator_node.h"
#include "graph/data_search.h"
#include "graph/hasher_node.h"
#include "graph/node.h"

namespace graph
{

struct Graph
{
    using DynamicNode = std::unique_ptr<Node>;

    Graph& add( DynamicNode aNode );
    Graph& add( const std::string& aIdNode );
    Graph& addConnection( const std::string& aIdFirst, const ConnectionHalf& aConnection );

    const DynamicNode& get( const std::string& aIdNode );
    std::string        toString( );

    void searchDFS( DataSearch& aDataSearch );
    void searchBFS( DataSearch& aDataSearch );

    static std::unique_ptr<Graph> CreateGraph( std::vector<std::vector<uint8_t>>& aGraphMatrix );

private:
    void searchDFS( const ConnectionHalf& aConnection, DataSearch& dataSearch, Solution aSolution );

    std::unordered_set<DynamicNode, HasherNode, ComparatorNode> mNodes;
};

} // namespace graph
