#pragma once
#include "graph/node.h"

namespace graph {

struct HasherNode
{
  std::size_t operator( )( const graph::Node& aNode ) const
  {
    return std::hash<std::string>( )( aNode.getId( ) );
  }

  std::size_t operator( )( const std::unique_ptr<graph::Node>& aNode ) const
  {
    return std::hash<std::string>( )( aNode->getId( ) );
  }
};

} // namespace graph
