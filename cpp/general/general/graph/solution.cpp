#include "graph/solution.h"

namespace graph
{

Solution::Solution( const std::string& aIdNodeStart )
    : mIdNodeStart {aIdNodeStart}
{
}

void Solution::addConnection( const ConnectionHalf& aConnection )
{
    mValue += aConnection.getValue( );
    mConnections.push_back( aConnection );
}

const std::vector<ConnectionHalf>& Solution::getConnections( ) const { return mConnections; }

uint32_t Solution::getValue( ) const { return mValue; }

std::string Solution::toString( ) const
{
    std::string strRepresentation = "Solution: " + mIdNodeStart + " ";
    for ( const auto& connection : mConnections )
    {
        strRepresentation += connection.getIdDestination( ) + " ";
    }
    strRepresentation += std::to_string( mValue );

    return strRepresentation;
}

} // namespace graph
