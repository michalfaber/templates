#include "graph/connection_half.h"

namespace graph
{

ConnectionHalf::ConnectionHalf(const std::string &aIdDestination, const uint32_t aValue)
    : mIdNodeDestination{ aIdDestination }
    , mValue{ aValue } {}

const std::string &ConnectionHalf::getIdDestination() const
{
    return mIdNodeDestination;
}

uint32_t ConnectionHalf::getValue() const
{
    return mValue;
}

}
