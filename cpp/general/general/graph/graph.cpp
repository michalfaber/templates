#include "graph/graph.h"
#include <queue>
#include <algorithm>

namespace graph
{

Graph &Graph::add(Graph::DynamicNode aNode)
{
    if ( aNode )
    {
//        mNodes[aNode->getId()] = std::move( aNode );
        mNodes.insert( std::move( aNode ) );
    }
    return *this;
}

Graph &Graph::add(const std::string &aIdNode)
{
    mNodes.insert( Node::Create( aIdNode ) );
    return *this;
}

const Graph::DynamicNode &Graph::get(const std::string &aIdNode)
{
    auto iter = mNodes.find( graph::Node::Create( aIdNode) );
    if ( iter == mNodes.end() ) throw "Not found";
    return *iter;
}

Graph &Graph::addConnection(const std::string &aIdFirst, const ConnectionHalf &aConnection)
{
    get( aIdFirst )->addConnection( aConnection );
    return *this;
}

std::string Graph::toString()
{
    std::string strRep = "";
    for ( const auto& node : mNodes )
    {
        strRep += node->getId() + " :";
        for ( const auto& connection : node->getConnections() )
        {
            strRep += " " + connection.getIdDestination();
            strRep += " " + std::to_string( connection.getValue() ) + "; ";
        }
        strRep += "\n";
    }

    return strRep;
}

void Graph::searchDFS(DataSearch &aDataSearch)
{
    for ( const auto& connection : get( aDataSearch.mIdNodeToStart )->getConnections() )
    {
        searchDFS( connection, aDataSearch, Solution{ aDataSearch.mIdNodeToStart } );
    }
}

void Graph::searchBFS(DataSearch &aDataSearch)
{

    auto createPossibleSolutions = []( const Solution aSolutionInProgress
            , const std::vector<ConnectionHalf>& aConnections
            , std::queue<Solution>& aSolutions )
    {
        // 1. Copy aSolutionInProgress to solutionTemp
        // 2. Check if solutionTemp already contains destination
        // 3. If solutionTemp does not contain destination from connection,
        //    add connection to solutionTemp and add solutionTemp to queue aSolutions
        for ( const auto& connection : aConnections )
        {
            auto solutionTemp = aSolutionInProgress;
            const auto& connectionsInSolution = solutionTemp.getConnections();

            // check if solution already contains a connection.
            auto iter = std::find_if( connectionsInSolution.begin(), connectionsInSolution.end()
                                      , [&connection]( const ConnectionHalf& c )
            {
                return c.getIdDestination() == connection.getIdDestination();
            } );

            // if solutionInProgress does not contain connection,
            // create new possible solution and add it to queue.
            if ( iter == connectionsInSolution.end() )
            {
                solutionTemp.addConnection( connection );
                aSolutions.push( solutionTemp );
            }
        }
    };

    std::queue<Solution> possibleSolutions;
    createPossibleSolutions( Solution{ aDataSearch.mIdNodeToStart }, get( aDataSearch.mIdNodeToStart )->getConnections(), possibleSolutions );

    while ( !possibleSolutions.empty() )
    {
        const auto& solution = possibleSolutions.front();
        if ( solution.getConnections().empty() )
        {
            possibleSolutions.pop();
            continue;
        }

        if ( aDataSearch.mIdNodeToFind == solution.getConnections().back().getIdDestination() )
        {
            aDataSearch.addSolution( solution );
            possibleSolutions.pop();
            continue;
        }

        createPossibleSolutions( solution, get( solution.getConnections().back().getIdDestination() )->getConnections(), possibleSolutions);
        possibleSolutions.pop();
    }
}

std::unique_ptr<Graph> Graph::CreateGraph(std::vector<std::vector<uint8_t> > &aGraphMatrix)
{
    auto graph = std::make_unique<Graph>();

    size_t x = 0, y = 0;

    for ( auto const& row : aGraphMatrix )
    {
        for ( auto const& element : row )
        {
            auto idNode = std::string( "y" ) + std::to_string( y ) +
                    std::string( "x" ) + std::to_string( x );
            auto currentNode = Node::Create( idNode );

            if ( x != 0 && element ) // element with value 0 is treated as wall and should not be connected
            {
                // add itselfs as right neighbor
                auto idNodeLeft = std::string( "y" ) + std::to_string( y ) +
                        std::string( "x" ) + std::to_string( x - 1 );
                auto& nodeLeft = graph->get( idNodeLeft );
                if ( aGraphMatrix[y][x - 1] )
                {
                    nodeLeft->addConnection( ConnectionHalf{ currentNode->getId(), aGraphMatrix[y][x] } );
                    currentNode->addConnection( ConnectionHalf{ idNodeLeft, aGraphMatrix[y][x - 1] } );
                }
            }

            if ( y != 0 && element ) // element with value 0 is treated as wall and should not be connected
            {
                // add itselfs as bottom neighbor
                auto idNodeUpper = std::string( "y" ) + std::to_string( y - 1 ) +
                        std::string( "x" ) + std::to_string( x );

                auto& nodeUpper = graph->get( idNodeUpper );
                if ( aGraphMatrix[y - 1][x] )
                {
                    nodeUpper->addConnection( ConnectionHalf{ currentNode->getId(), aGraphMatrix[y][x] } );
                    currentNode->addConnection( ConnectionHalf{ idNodeUpper, aGraphMatrix[y - 1][x] } );
                }
            }

            graph->add( std::move( currentNode ) );
            ++x;
        }
        x = 0;
        ++y;
    }

    return graph;
}

void Graph::searchDFS(const ConnectionHalf &aConnection, DataSearch &dataSearch, Solution aSolution)
{
    const auto& aVisited = aSolution.getConnections();
    auto iterFind = std::find_if( aVisited.begin(), aVisited.end(), [&aConnection]( const ConnectionHalf& connection )
    {
        return connection.getIdDestination() == aConnection.getIdDestination();
    } );

    if ( iterFind != aVisited.cend() )
    {
        // omitting a case with a cycle in graph
        return;
    }

    aSolution.addConnection( aConnection );
    if ( aConnection.getIdDestination() == dataSearch.mIdNodeToFind )
    {
        dataSearch.addSolution( aSolution );
        return;
    }

    for ( const auto& connection: get( aConnection.getIdDestination() )->getConnections() )
    {
        searchDFS( connection, dataSearch, aSolution );
    }
}

}
