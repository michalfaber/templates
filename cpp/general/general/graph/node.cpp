#include "graph/node.h"

namespace graph
{

std::unique_ptr<Node> Node::Create(const std::string &id)
{
    return std::make_unique<Node>( id );
}

Node::Node(const std::string &id) : mId{ id } {}

Node::~Node()
{
    std::cout << __PRETTY_FUNCTION__ << " " << mId << " " << std::endl;
}

const std::string &Node::getId() const
{
    return mId;
}

void Node::addConnection(const ConnectionHalf &aConnection)
{
    mConnections.push_back( aConnection );
}

const std::vector<ConnectionHalf> &Node::getConnections() const
{
    return mConnections;
}

}
