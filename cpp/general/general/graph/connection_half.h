#pragma once
#include <string>

namespace graph
{

struct ConnectionHalf
{
    ConnectionHalf( const std::string& aIdDestination, const uint32_t aValue );
    const std::string& getIdDestination( ) const;
    uint32_t           getValue( ) const;

private:
    std::string mIdNodeDestination;
    uint32_t    mValue;
};

}
