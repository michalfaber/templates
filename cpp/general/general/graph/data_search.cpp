#include "graph/data_search.h"

namespace graph
{

DataSearch::DataSearch()
{
    mSolutionBestOne.addConnection( ConnectionHalf{ "", std::numeric_limits<uint32_t>::max() } );
}

void DataSearch::addSolution(const Solution &aSolution)
{
    mSolutions.push_back( aSolution );

    if ( mSolutionBestOne.getValue() > aSolution.getValue() )
    {
        mSolutionBestOne = aSolution;
    }
}

}
