#include "graph/connection_full.h"

namespace graph {

ConnectionFull::ConnectionFull( const std::string& aIdNodeStart,
                                const std::string& aIdDestination,
                                const uint32_t     aValue )
    : ConnectionHalf( aIdDestination, aValue )
    , mIdNodeStart {aIdNodeStart}
{}

const std::string& ConnectionFull::getIdNodeStart( ) { return mIdNodeStart; }

} // namespace graph
