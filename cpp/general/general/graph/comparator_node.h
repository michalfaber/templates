#pragma once
#include "graph/node.h"

namespace graph {

struct ComparatorNode
{
  // The const qualifiers are relevant.

  bool operator( )( const Node& aNodeLeft, const Node& aNodeRight ) const
  {
    return aNodeLeft.getId( ) == aNodeRight.getId( );
  }

  bool operator( )( const std::unique_ptr<Node>& aNodeLeft, const std::unique_ptr<Node>& aNodeRight ) const
  {
    if ( ! aNodeLeft )
      return true;
    if ( ! aNodeRight )
      return false;

    return aNodeLeft->getId( ) == aNodeRight->getId( );
  }
};

}  // namespace graph
