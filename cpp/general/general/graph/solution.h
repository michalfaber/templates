#pragma once
#include "connection_half.h"
#include <vector>

namespace graph
{

struct Solution
{
    Solution( const std::string& aIdNodeStart = "" );

    void        addConnection( const ConnectionHalf& aConnection );
    std::string toString( ) const;
    uint32_t    getValue( ) const;

    const std::vector<ConnectionHalf>& getConnections( ) const;

private:
    std::string                 mIdNodeStart = "";
    std::vector<ConnectionHalf> mConnections {};
    uint32_t                    mValue = 0;
};

}
