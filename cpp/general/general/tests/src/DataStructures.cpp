#include <iostream>
#include <functional>
#include <memory>
#include <list>
#include <vector>
#include <algorithm>
#include <gtest/gtest.h>

/// Tree

template <typename T>
struct Node
{
    Node( const T& aValue ) : mValue{ aValue } {}

    Node<T>* setLeft( Node<T>& aNode )
    {
        mLeft          = &aNode;
        mLeft->mParent = this;
        return this;
    }

    Node<T>* setRight( Node<T>& aNode )
    {
        mRight          = &aNode;
        mRight->mParent = this;
        return this;
    }

    Node<T>* mParent = nullptr;
    Node<T>* mLeft   = nullptr;
    Node<T>* mRight  = nullptr;
    T        mValue;
};

template <typename U>
struct PreOrderIterator
{
    explicit PreOrderIterator( Node<U>* aCurrent ) : mCurrent{ aCurrent } {}

    bool operator!=( PreOrderIterator<U>& other ){ return mCurrent != other.mCurrent; }
    Node<U>& operator*()                         { return *mCurrent; }

    PreOrderIterator<U>& operator++()
    {
        if ( mCurrent->mRight )
        {
            mCurrent = mCurrent->mRight;
            while ( mCurrent->mLeft ) mCurrent = mCurrent->mLeft;
        }
        else
        {
            auto parent = mCurrent->mParent;
            while ( parent && mCurrent == parent->mRight )
            {
                mCurrent = parent;
                parent   = parent->mParent;
            }
            mCurrent = parent;
        }

        return *this;
    }

    Node<U>* mCurrent;
};

template <typename T>
struct Tree
{
    using iterator = PreOrderIterator<T>;
    enum class Traverse
    {
        InOrder,
        PreOrder,
        PostOrder
    };

    explicit Tree( Node<T>* aRoot ) : mRoot{ aRoot } { }

    iterator begin()
    {
        auto n = mRoot;
        if ( n ) while ( n->mLeft ) n = n->mLeft;

        return iterator{ n };
    }

    iterator end(){ return iterator{ nullptr }; }

    void travers( Traverse way, std::function<void(Node<T>&)> aFun )
    {
        if (      Traverse::InOrder   == way ) traversInOrder(   Tree<T>::mRoot, aFun );
        else if ( Traverse::PreOrder  == way ) traversPreOrder(  Tree<T>::mRoot, aFun );
        else if ( Traverse::PostOrder == way ) traversPostOrder( Tree<T>::mRoot, aFun );
    }

protected:
    Node<T>* mRoot;

private:
    void traversInOrder(Node<T>* node, std::function<void(Node<T>&)>& doSth)
    {
        if (!node) return;
        traversInOrder(node->mLeft, doSth);
        doSth(*node);
        traversInOrder(node->mRight, doSth);
    }

    void traversPreOrder(Node<T>* node, std::function<void(Node<T>&)>& doSth)
    {
        if (!node) return;
        doSth(*node);
        traversInOrder(node->mLeft, doSth);
        traversInOrder(node->mRight, doSth);
    }

    void traversPostOrder(Node<T>* node, std::function<void(Node<T>&)>& doSth)
    {
        if (!node) return;
        traversInOrder(node->mLeft, doSth);
        traversInOrder(node->mRight, doSth);
        doSth(*node);
    }
};

template<typename T>
struct DynamicBinaryTree : public Tree<T>
{
    DynamicBinaryTree() : Tree<T>{ nullptr } {}
    ~DynamicBinaryTree(){ deleteTree( Tree<T>::mRoot ); }

    void deleteTree( Node<T>* node )
    {
        if ( !node ) return;
        deleteTree( node->mLeft );
        deleteTree( node->mRight );
        delete node;
        node = nullptr;
    }

    DynamicBinaryTree& add( T aValue )
    {
        Node<T>* newNode = new Node<T>{ aValue };
        return add( newNode );
    }

    DynamicBinaryTree& add( Node<T>* element )
    {
        if ( !Tree<T>::mRoot )
        {
            Tree<T>::mRoot = element;
            return *this;
        }

        auto temp = Tree<T>::mRoot;
        while ( 1 )
        {
            if ( temp->mValue < element->mValue )
                if ( temp->mRight ) temp = temp->mRight;
                else
                {
                    temp->setRight( *element );
                    break;
                }
            else
                if ( temp->mLeft ) temp = temp->mLeft;
                else
                {
                    temp->setLeft(*element);
                    break;
                }
        }

        return *this;
    }
};

TEST( DataStructure, tree )
{
    Node<uint32_t> node[8] = { Node<uint32_t>{ 0 }
                             , Node<uint32_t>{ 1 }
                             , Node<uint32_t>{ 2 }
                             , Node<uint32_t>{ 3 }
                             , Node<uint32_t>{ 4 }
                             , Node<uint32_t>{ 5 }
                             , Node<uint32_t>{ 6 }
                             , Node<uint32_t>{ 7 } };
    node[0].setLeft(  node[1] )->setRight( node[2] );
    node[1].setLeft(  node[3] )->setRight( node[5] );
    node[2].setRight( node[4] );
    node[5].setRight( node[6] )->setLeft( node[7] );
    Tree<uint32_t> tree{ &node[0] };
    for ( auto& node : tree ) std::cout << "value: " << node.mValue << std::endl;

    DynamicBinaryTree<uint32_t> dynamicBinaryTree{};
    dynamicBinaryTree.add(5).add(1).add(3).add(7);

    for ( auto& node : dynamicBinaryTree ) std::cout << "value: " << node.mValue << std::endl;

    auto printVaule = []( Node<uint32_t>& node ){ std::cout << "travers: " << node.mValue << std::endl; };
    dynamicBinaryTree.travers( Tree<uint32_t>::Traverse::InOrder,   printVaule ); std::cout << std::endl;
    dynamicBinaryTree.travers( Tree<uint32_t>::Traverse::PreOrder,  printVaule ); std::cout << std::endl;
    dynamicBinaryTree.travers( Tree<uint32_t>::Traverse::PostOrder, printVaule ); std::cout << std::endl;
}


/// Stack

template <typename T>
class MyStack
{
public:
    T& front()
    {
        if (!mFront) throw "Empty stack";
        return mFront->mData;
    }

    template<typename TArg>
    MyStack& add( TArg&& aData )
    {
        auto newNode = StackNode::createNode( aData );
        if ( !newNode ) throw "Bad allocation of a node";

        if ( mFront) newNode->mNext = mFront;
        mFront = newNode;

        return *this;
    }

    T pop()
    {
        if ( !mFront ) throw "Empty stack";

        T returnValue = mFront->mData;
        mFront = mFront->mNext;

        return returnValue;
    }

private:
    struct StackNode
    {
        StackNode(T&  data) : mData{data} {}
        StackNode(T&& data) : mData{data} {}
        std::shared_ptr<StackNode> mNext;
        T mData;

    public:
        template<typename TArg>
        static std::shared_ptr<StackNode> createNode( TArg&& aData ) { return std::make_shared<StackNode>( aData ); }
    };

    std::shared_ptr<StackNode> mFront;
};

TEST( DataStructure, stack )
{
    MyStack<uint32_t> myStack;
    uint32_t intValue = 3;
    myStack.add(0).add(1).add(2).add(intValue).add(4);
    myStack.front() = 105;

    try
    {
        while (1) std::cout << myStack.pop() << std::endl;
    } catch ( const char* e )
    {
        std::cout << e << std::endl;
    }
}


/// Queue

template <typename T>
class MyQueue
{
public:
    T& front()
    {
        if ( !mFront ) throw "Empty stack";
        return mFront->mData;
    }

    template<typename TArg>
    MyQueue& add( TArg&& aData )
    {
        auto newNode = QueueNode::createNode( aData );
        if ( !newNode ) throw "Bad allocation of a node";

        if ( mBack ) mBack->mNext = newNode;
        mBack = newNode;

        if ( !mFront ) mFront = mBack;

        return *this;
    }

    T pop()
    {
        if ( !mFront ) throw "Empty stack";

        T returnValue = mFront->mData;
        mFront = mFront->mNext;

        return returnValue;
    }

private:
    struct QueueNode
    {
        QueueNode( T&  aData ) : mData{ aData } {}
        QueueNode( T&& aData ) : mData{ aData } {}
        std::shared_ptr<QueueNode> mNext;
        T mData;

    public:
        template<typename TArg>
        static std::shared_ptr< QueueNode > createNode( TArg&& aData ) { return std::make_shared<QueueNode>( aData ); }
    };

    std::shared_ptr<QueueNode> mFront;
    std::shared_ptr<QueueNode> mBack;
};

TEST( DataStructure, queue )
{
    MyQueue<uint32_t> myQueue;
    uint32_t intValue = 3;
    myQueue.add(0).add(1).add(2).add(intValue).add(4);
    myQueue.front() = 105;

    try
    {
        while (1) std::cout << myQueue.pop() << std::endl;
    } catch (const char* e)
    {
        std::cout << e << std::endl;
    }
}