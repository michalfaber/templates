#include "include/Classes.hpp"
#include "gtest/gtest.h"

void PureVirtual::methodDoSth()
{
    std::cout << __PRETTY_FUNCTION__ << std::endl;
}

template<>
int Storage<int>::getValue() const { return 5; }

Amove funTestAmove( Amove amove ) { return amove; }

ClassWithoutNoexcept returnClassWithoutNoexcept(){ return ClassWithoutNoexcept(); }
ClassWithNoexcept returnClassWithNoexcept() { return ClassWithNoexcept(); }

Degree operator"" _deg (long double deg) { return Degree{deg}; }

TEST( IsDerivative, Animalas )
{
    std::cout << IsDerivative<Dog, Animal>() << std::endl;
    std::cout << IsDerivative<Dog, Cow>() << std::endl;

    // Dog dog(); // Declaration of function dog which returns Dog
    Dog dog{}; // Ctor Dog
    dog.speak();
}

TEST( PureVirtual, withMethodDefinition )
{
    PureVirtualDerivative pureVirtualDerivative{};
    pureVirtualDerivative.methodDoSth();
    pureVirtualDerivative.PureVirtual::methodDoSth();
}

TEST( Template, methodSpecialization )
{
    Storage<double> storageDouble{99.11111111111};
    std::cout << "Value: " << storageDouble.getValue() << std::endl;

    Storage<uint32_t> storageInt{99};
    std::cout << "Value: " << storageInt.getValue()
                <<" trolololo :v!!! "<< std::endl;

    // TIDBIT
    // This below will not compile because of narrowing conversion.
    // These {} brackets makes assignment narrowing sensitive.
    //      Storage<uint32_t> storageInt{99.1111111111};    ERROR
    // but with rounded brackets there will be only warning during compilation
    //      Storage<uint32_t> storageInt(99.1111111111);    WARNING
}

TEST( SmartPointer, withClassObject )
{
    try
    {
        std::unique_ptr<Figure> uPtrFigureBadWay{ new Square{1.8} }; // Not safe!!!
        std::unique_ptr<Figure> uPtrFigureBetterWay = std::make_unique<Square>(1.8);
    }
    catch (std::bad_alloc& e) // from smart_ptr
    {
        std::cout << e.what() << std::endl;
    }
    catch (...)
    {
        std::cout << "unknown exception" << std::endl;
    }
}

TEST( MoveConstructor, andMoveOperator )
{
    Amove amove1("start value 1");
    Amove amove2 = amove1;
    Amove amove3 = std::move(amove1);
    Amove amove4 = funTestAmove(amove1);
    Amove amove5("start vaule 5");
    Amove amove6("start vaule 6");
    Amove amove7("start vaule 7");
    amove5 = amove1;
    amove6 = std::move(amove1);
    amove7 = funTestAmove(amove1);

    std::cout << "amove1: " << amove1.get_s() << std::endl
                << "amove2: " << amove2.get_s() << std::endl
                << "amove3: " << amove3.get_s() << std::endl
                << "amove4: " << amove4.get_s() << std::endl
                << "amove5: " << amove5.get_s() << std::endl
                << "amove6: " << amove6.get_s() << std::endl
                << "amove7: " << amove7.get_s() << std::endl;
}

TEST( CopyConstructor, example )
{
            std::cout << R"delimiter(
                     /// copy operator
                     )delimiter" << std::endl;

        BaseClassOpCopy  baseClassOpCopy;
        baseClassOpCopy.mValueFromBase         = 101;
        DerivativeClass1 derivativeClass1;
        derivativeClass1.mValueFromBase        = 201;
        derivativeClass1.mValueFromDerivative1 = 202;
        DerivativeClass2 derivativeClass2;
        derivativeClass2.mValueFromBase        = 301;
        derivativeClass2.mValueFromDerivative1 = 302;
        derivativeClass2.mValueFromDerivative2 = 303;

        BaseClassOpCopy& refBase1 = baseClassOpCopy;
        BaseClassOpCopy& refBase2 = derivativeClass1;
        BaseClassOpCopy& refBase3 = derivativeClass2;

        std::cout << refBase3.mValueFromBase << " " << derivativeClass2.mValueFromDerivative2 << std::endl;
        refBase3 = refBase2;
        std::cout << refBase3.mValueFromBase << " " << derivativeClass2.mValueFromDerivative2 << std::endl;
        refBase3 = baseClassOpCopy;
        std::cout << refBase3.mValueFromBase << " " << derivativeClass2.mValueFromDerivative2 << std::endl;
        derivativeClass1 = derivativeClass2;
        std::cout << derivativeClass1.mValueFromBase << " " << derivativeClass1.mValueFromDerivative1 << std::endl;
}

TEST( MoveConstructor, markedNoexcept )
{
    std::vector<ClassWithoutNoexcept> vectorClassWithoutNoexcept;
    std::vector<ClassWithNoexcept>    vectorClassWithNoexcept;
    vectorClassWithoutNoexcept.push_back(ClassWithoutNoexcept{});
    vectorClassWithNoexcept.push_back(ClassWithNoexcept{});

    std::cout << "\nStart, pls notice slightly but very important difference "
                    "related to noexcept in class definitions\n";
    vectorClassWithoutNoexcept.resize(3);
    vectorClassWithNoexcept.resize(3);

    // The point is that when ctor&& is marked as noexcept then
    // compiler is more likely to use it instead of ctor&

    std::cout << "\nFurther fun :)\n";
    vectorClassWithoutNoexcept.reserve(5);
    vectorClassWithNoexcept.reserve(5);
}

TEST( Units, example )
{
    std::cout << R"delimiter(
                    /// Defining units
                    )delimiter" << std::endl;

    auto degree = 91.0_deg;
    ++degree;
    std::cout << "Degree: " << degree.get() << std::endl;
}