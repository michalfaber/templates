#include "gtest/gtest.h"
#include <vector>
#include <string>

/// Game board - variation of graph where map is in form of matrix with values.
/// Every value in matrix represents cost of stepping on such field. Field with 0 means
/// that it is kind of wall or lava that can not be reached.

struct Point
{
    bool operator==( const Point& aPoint ) const
    {
        return ( this->x == aPoint.x ) && ( this->y == aPoint.y );
    }

    size_t x = 0,
           y = 0;
};

struct Connection
{
    Connection( const Point& aPoint, uint32_t aValue ) : mPoint{ aPoint }, mValue{ aValue } {}

    const Point& getPoint() const
    {
        return mPoint;
    }

    uint32_t getValue() const
    {
        return mValue;
    }

    std::string toString() const
    {
        std::string strRepresentation = "[";
        strRepresentation += std::to_string( mPoint.x ) + ",";
        strRepresentation += std::to_string( mPoint.y ) + "] ";
        strRepresentation += std::to_string( mValue )   + ";";
        return strRepresentation;
    }

private:
    Point  mPoint;
    size_t mValue;
};

struct Solution
{
    void addConnection( const Connection& aConnection )
    {
        mConnections.push_back( aConnection );
        mValue += aConnection.getValue();
    }

    const std::vector<Connection>& getConnections() const
    {
        return mConnections;
    }

    uint32_t getValue() const
    {
        return mValue;
    }

    bool contains( const Point& aPoint )
    {
        const auto iter = std::find_if( mConnections.begin(), mConnections.end(), [&aPoint]( const Connection& c )
        {
            return aPoint == c.getPoint();
        } );

        return iter != mConnections.end();
    }

    std::string toString()
    {
        std::string strRepresentation = "Solution: ";
        for ( const auto& connection : mConnections )
        {
            strRepresentation += connection.toString() + " ";
        }
        strRepresentation += " all: " + std::to_string( mValue );
        return strRepresentation;
    }

private:
    std::vector<Connection> mConnections;
    uint32_t                mValue = 0;
};

struct DataSearch
{
    DataSearch()
    {
        mSolutionBestOne.addConnection( { { 0, 0 }, std::numeric_limits<uint32_t>::max() } );
    }

    void addSolution( const Solution& aSolution )
    {
        mSolutions.push_back( aSolution );
        if ( aSolution.getValue() < mSolutionBestOne.getValue() )
        {
            mSolutionBestOne = aSolution;
        }
    }

    Point                 mStart;
    Point                 mDestination;
    std::vector<Solution> mSolutions;
    Solution              mSolutionBestOne;
};

class GameBoard
{
public:
    GameBoard( const std::vector<std::vector<uint32_t>>& aMatrix ) : mMatrix{ aMatrix } {}

    void search( DataSearch& aDataSearch ) const
    {
        searchDFS( { aDataSearch.mStart.x - 1, aDataSearch.mStart.y     }, aDataSearch, Solution{} );
        searchDFS( { aDataSearch.mStart.x + 1, aDataSearch.mStart.y     }, aDataSearch, Solution{} );
        searchDFS( { aDataSearch.mStart.x    , aDataSearch.mStart.y - 1 }, aDataSearch, Solution{} );
        searchDFS( { aDataSearch.mStart.x    , aDataSearch.mStart.y + 1 }, aDataSearch, Solution{} );
    }

    std::string toString()
    {
        std::string strRepresentation = "";
        for ( const auto& row : mMatrix )
        {
            for ( const auto& value : row )
            {
                strRepresentation += std::to_string( value ) + " ";
            }
            strRepresentation += "\n";
        }
        return strRepresentation;
    }

private:

    void searchDFS( const Point& aCurrentPoint, DataSearch& aDataSearch, Solution aPossibleSolution ) const
    {
        if ( !isInsideMatrix( aCurrentPoint ) )
        {
            // aCurrentPoint out of matrix range
            return;
        }

        if ( aPossibleSolution.contains( aCurrentPoint ) )
        {
            // aPossibleSolution solution already contains such point.
            // Skip this one to prevent looping or going into cycle.
            return;
        }

        if ( 0 == mMatrix[aCurrentPoint.y][aCurrentPoint.x] )
        {
            // can not step on filed with 0 value. The 0 means that it is kind of wall or lava.
            return;
        }

        aPossibleSolution.addConnection( { aCurrentPoint, mMatrix[aCurrentPoint.y][aCurrentPoint.x] } );

        if ( aCurrentPoint == aDataSearch.mDestination )
        {
            // a solution has been found
            aDataSearch.addSolution( aPossibleSolution );
            return;
        }

        searchDFS( { aCurrentPoint.x - 1, aCurrentPoint.y     }, aDataSearch, aPossibleSolution );
        searchDFS( { aCurrentPoint.x + 1, aCurrentPoint.y     }, aDataSearch, aPossibleSolution );
        searchDFS( { aCurrentPoint.x    , aCurrentPoint.y - 1 }, aDataSearch, aPossibleSolution );
        searchDFS( { aCurrentPoint.x    , aCurrentPoint.y + 1 }, aDataSearch, aPossibleSolution );
    }

    bool isInsideMatrix( const Point& aPoint ) const
    {
        bool isInside = true;

        if ( 0 > aPoint.y || aPoint.y >= mMatrix.size() )
        {
            isInside = false;
        }
        else
        {
            if ( 0 > aPoint.x || aPoint.x >= mMatrix[aPoint.y].size() )
            {
                isInside = false;
            }
        }

        return isInside;
    }

    std::vector<std::vector<uint32_t>> mMatrix;
};

TEST( GameBoard, search )
{
    GameBoard gameBoard
    {
        {
            { 1, 7, 2, 9 },
            { 9, 7, 1, 3 },
            { 0, 0, 0, 5 },
            { 1, 1, 7, 1 },
            { 0, 1, 1, 3 }
        }
    };

    DataSearch dataSearch;
    dataSearch.mStart       = { 0, 0 };
    dataSearch.mDestination = { 0, 3 };

    gameBoard.search( dataSearch );
    std::cout << dataSearch.mSolutionBestOne.toString() << std::endl;
    std::cout << gameBoard.toString() << std::endl;

}
