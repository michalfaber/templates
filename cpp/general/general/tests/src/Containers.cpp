#include <iostream>
#include <iomanip>
#include <algorithm>
#include <iterator>
#include <memory>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <unordered_set>
#include <unordered_map>
#include <functional>
#include <numeric>
#include <gtest/gtest.h>

#include "../common/Figure.hpp"

TEST( Containers, iterate )
{
    // should be in try catch block because of std::bad_alloc
    std::vector<std::shared_ptr<uint32_t>> vector1;
    vector1.push_back( std::make_shared<uint32_t>( 0 ) );
    vector1.push_back( std::make_shared<uint32_t>( 1 ) );
    vector1.push_back( std::make_shared<uint32_t>( 2 ) );

    // Old style, fuuu
    for ( auto iter = vector1.cbegin(); iter != vector1.cend(); ++iter )
    {
        std::cout << std::setw( 4)  << **iter << " ";
    }
    std::cout << std::endl;

    // Operation on element maybe specified in argument. Here it is done by lambda expression.
    // Also worth notice that shared_ptr must be const because of const iter. Reference is not needed.
    for_each (vector1.cbegin(), vector1.cend(), [](const std::shared_ptr<uint32_t>& ptr) 
    {
        std::cout << std::setw(4) << *ptr << " ";
    } );
    std::cout << std::endl;

    // Reverse order of iterating.
    for_each ( vector1.crbegin(), vector1.crend(), [](const std::shared_ptr<uint32_t>& ptr) 
    {
        std::cout << std::setw(4) << *ptr << " ";
    } );
    std::cout << std::endl;

    for ( const auto &ptr : vector1 )
    {
        // This is possible even when smart_ptr is const because the type held by it is not marked as const.
        *ptr = 5;
        std::cout << std::setw(4) << *ptr << " ";
    }
    std::cout << std::endl;

}

TEST( Containers, sort )
{
    auto printContainer = []( const auto& aContainer )
    { 
        for ( const auto& number : aContainer ) { std::cout << number << " "; } 
        std::cout << '\n';
    };

    std::array<uint32_t , 5> arrayA = {4, 3, 2, 1, 0};
    
    std::sort( arrayA.begin(), arrayA.end(), [](uint32_t a, uint32_t b) // ascending
    {
        return a < b;
    } );
    
    printContainer( arrayA );

    struct
    {
        bool operator()( int a, int b ) { return a > b; }
    } descending;
    std::sort(arrayA.begin(), arrayA.end(), descending);
    
    printContainer( arrayA );
}

TEST( Containers, remove )
{
    std::vector<uint32_t > vectorA = {3, 4, 9, 1, 1, 3, 1, 3, 2, 2, 1, 1, 1, 4};
    for (const auto& v : vectorA)
    {
        std::cout << v << " ";
    }
    std::cout << '\n';

    // Removes all 1 from range begin() end().
    // Notice that there are two times vectorA.end()
    vectorA.erase(remove(vectorA.begin(), vectorA.end(), 1), vectorA.end());
    for (const auto& v : vectorA)
    {
        std::cout << v << " ";
    }
    std::cout << '\n';

    // Notice that there is only one vectorA.end()
    // In this case only first occurrence of 2 will be erased from vectorA
    // Bad use of erase!
    vectorA.erase(remove_if(vectorA.begin(), vectorA.end(), [](int v)
                        {
                            return v == 2;
                        }
    ));
    for (auto v : vectorA)
    {
        std:: cout << v << " ";
    }
    std::cout << '\n';

    std::vector<uint32_t > vectorB(vectorA.size() - count(vectorA.begin(), vectorA.end(), 3));
    remove_copy(vectorA.begin(), vectorA.end(), vectorB.begin(), 3);
    vectorA = vectorB;
    for (auto v : vectorB)
    {
        std::cout << v << " ";
    }
    std::cout << '\n';

    vectorB.resize(vectorA.size() - count(vectorA.begin(), vectorA.end(), 4));
    remove_copy_if(vectorA.begin(), vectorA.end(), vectorB.begin(), [](int v)
                    {
                        return v == 4;
                    }
    );
    for (auto v : vectorB)
    {
        std::cout << v << " ";
    }
    std::cout << '\n';


    std::vector<std::shared_ptr<Figure>> vector1{ std::make_shared<Square>( 1.2 )
                                                , std::make_shared<Square>( 2.2 )
                                                , std::make_shared<Square>( 3.2 ) };
    for ( auto v: vector1)
    {
        std::cout << v->getSurface() << std::endl;
    }
    std::cout << std::endl;

    std::vector<std::shared_ptr<Figure>> vector2(
        vector1.size() - count_if( vector1.begin(), vector1.end(), [](std::shared_ptr<Figure> v)
            {
                return v->getSurface() < 10.0f;
            }
        )
    );
 
    remove_copy_if( vector1.begin(), vector1.end(), vector2.begin(), [](std::shared_ptr<Figure> v)
        {
            return v->getSurface() < 10.0f;
        }
    );
    vector1 = vector2;

    for (auto v: vector1)
    {
        std::cout << v->getSurface() << std::endl;
    }
    std::cout << std::endl;
}

TEST( Containers, map_and_set )
{
    std::map<const uint32_t, std::string> week;
    week[1] = "Monday";
    week[2] = "Tuesday";
    week[3] = "Wednesday";
    week[5] = "Thursday";
    week[6] = "Friday";
    week[7] = "Saturday";
    week[8] = "Sunday";
    week.insert(std::make_pair(9, "\textra value heh"));

    std::cout << "Third day of week: " << week[3]    << '\n';
    std::cout << "Fifth day of week: " << week[5]    << '\n';
    std::cout << "Extra day of week: " << week.at(9) << '\n';

    auto iter = week.begin();
    std::cout << iter->first << " " << iter->second << '\n';


    std::set<Square> set1;
    set1.insert(Square(2.1f));
    set1.insert(Square(1.1f));
    set1.insert(Square(2.1f));
    set1.insert(Square(-1.1f));

    uint32_t i = 0;
    for (const auto& s : set1)
    {
        // Notice set1 should contain only 2 objects.
        std::cout << i++ << ":  " << s.getSurface() << std::endl;
    };
}

TEST( Containers, unordered_set_square )
{
    std::unordered_set<Square> unorderedSet;
    unorderedSet.insert( Square(  1.2f ) );
    unorderedSet.insert( Square( -1.2f ) );
    unorderedSet.insert( Square( -2.3f ) );
    unorderedSet.insert( Square(  2.3f ) );

    for ( const auto &iter : unorderedSet )
    {
        std::cout << iter.getSide() << std::endl;
    }

    auto iter = unorderedSet.find( Square( 1.2f ) );
    std::cout << "Find: " << iter->getSide() << std::endl;
}

TEST( Containers, unordered_set_square_pointer )
{
    struct SquarePointerComparatorSide
    {
        bool operator() ( const Square* obj1, const Square* obj2 ) const
        {
            std::cout << __PRETTY_FUNCTION__ << std::endl;
            return obj1->getSide() == obj2->getSide();
        }
    };

    struct SquarePointerHasherSide
    {
        size_t operator() ( const Square* obj ) const
        {
            std::cout << __PRETTY_FUNCTION__ << std::endl;
            return ((51 + std::hash<float>()( obj->getSide() ) ) * 51 + std::hash<float>()( obj->getSide() ));
        }
    };

    std::unordered_set<Square *,
                       SquarePointerHasherSide,
                       SquarePointerComparatorSide> unorderedSet;
    unorderedSet.insert( new Square( 1.2f  ) );
    unorderedSet.insert( new Square( 2.2f  ) );
    unorderedSet.insert( new Square( -1.2f ) );
    unorderedSet.insert( new Square( -1.2f ) );

    for ( auto ptr : unorderedSet ) std::cout << ptr->getSide() << std::endl;
    for ( auto ptr : unorderedSet ) delete ptr;
}
  

TEST( Containers, unordered_set_square_smart_pointer )
{
    struct SquareSharedPointerComparatorSide
    {
        bool operator() ( const std::shared_ptr<Square>& obj1,
                          const std::shared_ptr<Square>& obj2) const
        {
            std::cout << __PRETTY_FUNCTION__ << std::endl;
            return obj1->getSide() == obj2->getSide();
        }
    };

    struct SquareSharedPointerHasherSide
    {
        size_t operator() ( const std::shared_ptr<Square>& obj ) const
        {
            std::cout << __PRETTY_FUNCTION__ << std::endl;
            return ( 51 + std::hash<float>()( obj->getSide() ) ) * 51 + std::hash<float>()( obj->getSide() );
        }
    };

    std::unordered_set<std::shared_ptr<Square>, 
                       SquareSharedPointerHasherSide, 
                       SquareSharedPointerComparatorSide> unorderedSet;
    unorderedSet.insert( std::make_shared<Square>(  1.2f ) );
    unorderedSet.insert( std::make_shared<Square>(  2.2f ) );
    unorderedSet.insert( std::make_shared<Square>( -1.2f ) );
    unorderedSet.insert( std::make_shared<Square>( -1.2f ) );

    for (const auto &iter : unorderedSet) std::cout << iter->getSide() << std::endl;
}

TEST( Containers, unordered_map_square_smart_pointer )
{
    struct SquareSharedPointerComparatorSide
    {
        bool operator() (const std::shared_ptr<Square>& obj1,
                            const std::shared_ptr<Square>& obj2) const
        {
            std::cout << __PRETTY_FUNCTION__ << std::endl;
            return obj1->getSide() == obj2->getSide();
        }
    };

    struct SquareSharedPointerHasherSide
    {
        size_t operator() (const std::shared_ptr<Square>& obj) const
        {
            std::cout << __PRETTY_FUNCTION__ << std::endl;
            return (
                    (51 + std::hash<float>()(obj->getSide())) * 51
                    + std::hash<float>()(obj->getSide()));
        }
    };

    std::unordered_map<std::shared_ptr<Square>,
                        uint32_t,
                        SquareSharedPointerHasherSide,
                        SquareSharedPointerComparatorSide> unorderedMap;
    unorderedMap.insert(std::pair<std::shared_ptr<Square>, uint32_t>(std::make_shared<Square>(1.2f), 1));
    unorderedMap.insert(std::pair<std::shared_ptr<Square>, uint32_t>(std::make_shared<Square>(2.2f), 2));
    unorderedMap.insert(std::pair<std::shared_ptr<Square>, uint32_t>(std::make_shared<Square>(1.2f), 1));

    for ( auto [ first, second ] : unorderedMap ) std::cout << first->getSide() << " : " << second << std::endl;
}

TEST( Containers, adjecent_difference )
{
    std::vector<uint32_t> v{1, 1, 1, 1, 1, 1, 1, 1, 1, 1};
    std::adjacent_difference(v.begin(), v.end() - 1, v.begin() + 1, std::plus<>());

    std::copy(v.begin(), v.end(), std::ostream_iterator<uint32_t>(std::cout, " "));
    std::cout << "\n";
}

TEST( Containers, iterator_validation )
{
    // std::vector::push_back
    // If the new size() is greater than capacity() then all iterators and references
    // (including the past-the-end iterator) are invalidated.
    // Otherwise only the past-the-end iterator is invalidated.
    // http://en.cppreference.com/w/cpp/container/vector/push_back

    // std::vector::insert
    // If the new size is greater than capacity, then all iterators and references are invalidated.
    // Otherwise, only iterators before insertion point remain valid.

    std::vector<uint32_t> vectorInts{0, 1, 2, 3, 4};
    auto iterEndVector = vectorInts.end();
    vectorInts.push_back(2);
    if (iterEndVector != vectorInts.end())
        std::cout << "iterEndVector - was invalidated!" << std::endl;

    auto iterEndVectorBeforeReserve = vectorInts.end();
    vectorInts.reserve(vectorInts.capacity() + 1);
    if (iterEndVectorBeforeReserve != vectorInts.end())
        std::cout << "iterEndVectorBeforeReserve - was invalidated!" << std::endl;


    // std::list::erase
    // References and iterators to the erased elements are invalidated.
    // Other references and iterators are not affected.

    std::list<uint32_t> listInts{0, 1, 2, 3, 4};
    auto iterEndList = listInts.end();
    listInts.push_back(2);

    if (iterEndList == listInts.end())
        std::cout << "iterEndList - OK!" << std::endl;

    auto iterToSecond = ++listInts.begin();
    std::cout << *iterToSecond << std::endl;
    listInts.insert(iterToSecond, 99);

    for (const auto& l : listInts) std::cout << l << " ";
    std::cout << std::endl;

    if (iterToSecond != ++listInts.begin())
        std::cout << "iterToSecond - was invalidated!" << std::endl;

}

TEST( Containers, emplace_vs_insert )
{
    class MyStr
    {
    public:
        MyStr( std::string&& str ) noexcept
        {
            std::cout << __PRETTY_FUNCTION__ << std::endl;
            _str = str;
        }
        ~MyStr() noexcept { std::cout << __PRETTY_FUNCTION__ << std::endl; }
        MyStr( const MyStr& obj ) noexcept
        {
            std::cout << __PRETTY_FUNCTION__ << std::endl;
            _str = obj._str;
        }
        MyStr( MyStr&& obj ) noexcept
        {
            std::cout << __PRETTY_FUNCTION__ << std::endl;
            _str = std::move(obj._str);
        }
        MyStr& operator=( const MyStr& obj )
        {
            std::cout << __PRETTY_FUNCTION__ << std::endl;
            _str = obj._str;
            return *this;
        }
        MyStr& operator=( MyStr&& obj ) noexcept
        {
            std::cout << __PRETTY_FUNCTION__ << std::endl;
            _str = std::move(obj._str);
            return *this;
        }

    private:
        std::string _str;
    };

    std::vector<MyStr> vectorMyStr{ MyStr( std::string( "First"  ) )
                                  , MyStr( std::string( "Second" ) )
                                  , MyStr( std::string( "Third"  ) ) };
    vectorMyStr.reserve(10);

    std::cout << "--- prepared ---" << std::endl;
    vectorMyStr.emplace_back("Fourth");
    std::cout << "--- after emplace_back ---" << std::endl;
    vectorMyStr.push_back(std::string("Fifth"));
    std::cout << "--- after push_back ---" << std::endl;

    // Difference in emplace_back compared to push_back
    // Instead of taking a value_type it takes a variadic list of arguments by r-value 
    // with possibility of  reference collapse, so that means that you can now 
    // perfectly forward the arguments and construct directly an object in a container
    //  without a temporary at all. There is invocation of placement new in memory of a container.
}

TEST( Containers, mismatch )
{
    //Returns the first mismatching pair of elements from two ranges

    std::string word1("abcdba");
    std::string word2("abcdef");

    std::function<std::string(const std::string&, const std::string&)> getCommonSubstring =
            [] (const std::string& str1, const std::string& str2) -> std::string
    {
        auto pairOfIterator = std::mismatch(str1.cbegin(), str1.cend(), str2.cbegin(),
                                            [](const char& a, const char& b) { return a == b; });
                                            // The lambda expr is not needed. Here just for presentation purpose.

        if (pairOfIterator.first != str1.cend())
        {
            std::cout << "str1: " << *pairOfIterator.first << std::endl;
        }
        if (pairOfIterator.second != str2.cend())
        {
            std::cout << "str2: " << *pairOfIterator.second << std::endl;
        }

        return std::string(str1.cbegin(), pairOfIterator.first);
    };

    std::cout << getCommonSubstring(word1, word2) << std::endl;
    std::cout << getCommonSubstring(word1, word1) << std::endl;
}

TEST( Containers, find_end_and_distance )
{
    std::vector<uint32_t> numbers{ 1, 2, 3, 4, 1, 2, 3, 4, 1, 2, 3, 4 };
    std::vector<uint32_t> sequenceToFind{ 1, 2, 3 };

    auto result = std::find_end( numbers.begin(), numbers.end(),
                                sequenceToFind.begin(), sequenceToFind.end());
    if ( result == numbers.end() )
        std::cout << "subsequence not found\n";
    else
        std::cout << "last subsequence is at: " << std::distance( numbers.begin(), result ) << "\n";

    std::vector<uint32_t> sequenceToFind2{ 4, 5, 6 };
    result = std::find_end( numbers.begin(), numbers.end(),
                            sequenceToFind2.begin(), sequenceToFind2.end());
    if ( result == numbers.end() )
        std::cout << "subsequence not found\n";
    else
        std::cout << "last subsequence is at: " << std::distance( numbers.begin(), result ) << "\n";
}

TEST( Containers, partial_sum)
{
    std::istringstream str("0.1 0.2 0.3 0.4");
    std::partial_sum(std::istream_iterator<double>(str),
                        std::istream_iterator<double>(),
                        std::ostream_iterator<double>(std::cout, " | | "));

    std::string s = "\n\nThis is an example\n";
    std::copy(s.begin(), s.end(), std::ostreambuf_iterator<char>(std::cout));
}


