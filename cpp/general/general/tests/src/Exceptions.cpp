#include "common/Figure.hpp"
#include <iostream>
#include <memory>
#include <vector>
#include <exception>
#include <gtest/gtest.h>

void fun_sneaky()
{
    throw __PRETTY_FUNCTION__;
}

void fun() noexcept
{
    fun_sneaky();
}

TEST( Exception, basicExample )
{
    try
    {
        Square square(1.3);
        throw 1;
    }
    catch (uint32_t value) {
        std::cout << "\tvalue: " << value << " was caught\n";
    }
    catch (...) {
        std::cout << "\tUnknown exception\n";
    }


    try {
        throw std::logic_error("msg: logic error");
    }
    catch (std::logic_error& e) {
        std::cout << e.what() << std::endl;
    }
    catch (...) {
        std::cout << "\tUnknown exception\n";
    }
}

TEST( Exception, nestedRethrow )
{
    try
    {
        try
        {
            throw static_cast<uint32_t>(15);
        }
        catch (std::bad_alloc& e) // from smart_ptr
        {
            std::cout << e.what() << std::endl;
        }
        catch (uint32_t& e) // from smart_ptr
        {
            std::cout << "level 2 - uint32_t: " << e << std::endl;

            throw static_cast<double>(114.0);
        }
        catch (...)
        {
            std::cout << "level 2 - unknown exception" << std::endl;
        }
    }
    catch (double& e)
    {
        std::cout << "level 1 - double" << e << std::endl;
    }
    catch (...)
    {
        std::cout << "level 1 - unknown exception" << std::endl;
    }
}

TEST( Exception, defineMyException )
{
    struct MyException : public std::exception  //Exactly MyException does'nt have to derive from std::exception
    {
        const char* what() const noexcept { return "Description of my exception"; }
    };

    try
    {
        throw MyException{};
    } catch( std::exception& e )
    {
        std::cout << "Caught as | std::exception | " << e.what() << std::endl;
    }
    catch( MyException& e)
    {
        std::cout << "Caught as | MyException | " << e.what() << std::endl;
    }
    catch ( ... )
    {
        std::cout << "Sth went wrong" << std::endl;
    }

}

TEST( Exception, inheritanceTree )
{
    struct MyException      // notice lack of inheritance from std::exception
    {
        virtual const char* what() const noexcept { return "My exception "; }
    };

    struct MyExceptionSpecial: public MyException
    {
        const char* what() const noexcept override { return "My exception special"; }
    };

    try
    {
        throw MyExceptionSpecial{};
    }
    catch( std::exception& e )
    {
        std::cout << "Caught as | std::exception | " << e.what() << std::endl;
    }
    catch( MyException& e )
    {
        std::cout << "Caught as | MyException | " << e.what() << std::endl;
    }
    catch( MyExceptionSpecial& e)
    {
        std::cout << "Caught as | MyExceptionSpecial | " << e.what() << std::endl;
    }
    catch ( ... )
    {
        std::cout << "Sth went wrong" << std::endl;
    }
}

TEST( Exception, DISABLED_useFunctionMarkedNothrow )
{
    // terminate because exception thrown by function that declares that does not throw exceptions
    try { fun(); }
    catch ( ... ) { }
}