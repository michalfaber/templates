#include <iostream>
#include <iomanip>
#include <memory>
#include <vector>
#include <algorithm>
#include "../../common/Figure.hpp"

#include "gtest/gtest.h"

/// Cycle reference with smart_ptr

class CycleReferenceGoodB;
class CycleReferenceGoodA
{
public:
    ~CycleReferenceGoodA() { std::cout << __PRETTY_FUNCTION__ << std::endl; }

    std::weak_ptr<CycleReferenceGoodB> m_sptrGoodB;
};

class CycleReferenceGoodB
{
public:
    ~CycleReferenceGoodB() { std::cout << __PRETTY_FUNCTION__ << std::endl; }

    std::weak_ptr<CycleReferenceGoodA> m_sptrGoodA;
};

class CycleReferenceBadB;
class CycleReferenceBadA
{
public:
    ~CycleReferenceBadA() { std::cout << __PRETTY_FUNCTION__ << std::endl; }

    std::shared_ptr<CycleReferenceBadB> m_sptrBadB;
};

class CycleReferenceBadB
{
public:
    ~CycleReferenceBadB() { std::cout << __PRETTY_FUNCTION__ << std::endl; }
    std::shared_ptr<CycleReferenceBadA> m_sptrBadA;
};

void showWhatUniquePtrHolds(std::unique_ptr<uint32_t> ptr){ std::cout << *ptr << std::endl; }

TEST( SmartPtr, unique_ptr_reset_and_move )
{
    try
    {
        // std::unique_ptr<Figure> uPtrFigure{ new Square{1.8} }; Not safe!!!
        std::unique_ptr<Figure> uPtrFigure = std::make_unique<Square>(1.8);
        std::unique_ptr<Figure> uPtrFigurePtr2 = std::move(uPtrFigure);
        std::cout << "Perimeter: " << uPtrFigurePtr2->getPerimeter() << "\n";
        uPtrFigurePtr2.reset();  // Reset invokes dereference and
        // destructor if reference count equals 0

        std::unique_ptr<uint32_t> uPtr, uPtr2;
        uPtr = std::make_unique<uint32_t>(3);
        showWhatUniquePtrHolds(std::move(uPtr));
        uPtr = std::make_unique<uint32_t>(4);
        uPtr2 = std::move(uPtr);
        // This below is not possible because copy constructor and copy operator is deleted for unique_ptr
        // showWhatUniquePtrHolds(uPtr);
        // uPtr2 = uPtr;
    }
    catch (std::bad_alloc& e) // from smart_ptr
    {
    std::cout << e.what() << std::endl;
    }
    catch (...)
    {
    std::cout << "unknown exception" << std::endl;
    }
}

TEST( SmartPtr, shared_ptr_use_count )
{
    try
    {
        auto sPtrFigure = std::make_shared<uint32_t >(1.8f);
        auto sPtrFigure2 = sPtrFigure;
        std::cout << "sPtrFigure2.use_count(): " << sPtrFigure2.use_count() << std::endl;
    }
    catch (std::bad_alloc& e) // from smart_ptr
    {
        std::cout << e.what() << std::endl;
    }
    catch (...)
    {
        std::cout << "unknown exception" << std::endl;
    }
}

TEST( SmartPtr, shared_ptr_cycle_reference )
{
    try
    {
        auto sptrGoodA = std::make_shared<CycleReferenceGoodA>();
        auto sptrGoodB = std::make_shared<CycleReferenceGoodB>();
        auto sptrBadA = std::make_shared<CycleReferenceBadA>();
        auto sptrBadB = std::make_shared<CycleReferenceBadB>();
        sptrGoodB->m_sptrGoodA = sptrGoodA;
        sptrGoodA->m_sptrGoodB = sptrGoodB;
        sptrBadA->m_sptrBadB = sptrBadB;
        // Uncomment below and whops there is no dtor: ~CycleReferenceBadA() ~CycleReferenceBadB()
        // This happens because the reference count has value of 2
        // and in this section there will one dereference associated with names: sptrBadA, sptrBadB
        // After that sptrBadB and sptrBadA still will have reference count equals 1.
        //sptrBadB->m_sptrBadA = sptrBadA;
    }
    catch (std::bad_alloc& e) // from smart_ptr
    {
        std::cout << e.what() << std::endl;
    }
    catch (...)
    {
        std::cout << "unknown exception" << std::endl;
    }
}

TEST( SmartPtr,  OneDimensionArrayUniquePtrAndVector )
{
    try
    {
        std::cout << "unique_ptr: " << std::endl;
        uint32_t numberOfElements = 3;
        std::unique_ptr<std::unique_ptr<Figure>[]> ptr2(new std::unique_ptr<Figure>[numberOfElements]);
        for (uint32_t i = 0; i < numberOfElements; ++i) {
            ptr2[i] = std::make_unique<Square>(i + 0.2f);
            std::cout << std::setw(6) << ptr2[i]->getSurface() << " ";
        }
        std::cout << std::endl;

        std::cout << "vector: " << std::endl;
        std::vector<std::unique_ptr<Figure>> vector1;
        for (uint32_t i = 0; i < numberOfElements; ++i) {
            //vector1.push_back(std::unique_ptr<Figure>(new Square(i + 0.2f))); bad way
            vector1.push_back(std::make_unique<Square>(i + 0.2f));
            std::cout << std::setw(6) << vector1[i]->getSurface() << " ";
        }
        std::cout << std::endl;
    }
    catch (std::bad_alloc& e) // from smart_ptr and vector
    {
        std::cout << e.what() << std::endl;
    }
    catch (...)
    {
        std::cout << "unknown exception" << std::endl;
    }
}

TEST( SmartPtr, TwoDimensionArrayUniquePtrAndVector )
{
    try
    {
        std::cout << "unique_ptr: " << std::endl;
        uint32_t numberOfElements = 3;
        std::unique_ptr<std::unique_ptr<std::unique_ptr<Figure>[]>[]> ptr(new std::unique_ptr<std::unique_ptr<Figure>[]>[numberOfElements]);
        for (uint32_t i = 0; i < numberOfElements; ++i)
        {
            ptr[i] = std::unique_ptr<std::unique_ptr<Figure>[]>(new std::unique_ptr<Figure>[numberOfElements]);
            for (uint32_t j = 0; j < numberOfElements; ++j)
            {
                ptr[i][j] = std::make_unique<Square>(i * 10 + j * 0.2f);
                std::cout << std::setw(6) << ptr[i][j]->getSurface() << " ";
            }
            std::cout << std::endl;
        }

        std::cout << "vector: " << std::endl;
        std::vector<std::vector<std::unique_ptr<Figure>>> vector1;
        for (uint32_t i = 0; i < numberOfElements; ++i)
        {
            vector1.push_back(std::vector<std::unique_ptr<Figure>>());
            for (uint32_t j = 0; j < numberOfElements; ++j)
            {
                vector1[i].push_back(std::make_unique<Square>(i * 10 + j * 0.2f));
                std::cout << std::setw(6) << vector1[i][j]->getSurface() << " ";
            }
            std::cout << std::endl;
        }
        vector1.clear();
    }
    catch (std::bad_alloc& e) // from smart_ptr and vector
    {
        std::cout << e.what() << std::endl;
    }
    catch (...)
    {
        std::cout << "unknown exception" << std::endl;
    }
}