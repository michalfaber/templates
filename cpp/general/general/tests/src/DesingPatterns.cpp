#include <iostream>
#include <memory>
#include <vector>
#include <map>
#include <functional>
#include <numeric>
#include <algorithm>
#include <iterator>
#include "gtest/gtest.h"

template<typename... TArgs>
struct Signal
{
    using TypeFunction = std::function< void( TArgs... ) >;
    Signal() = default;

    void connect( const TypeFunction& aFunction ){ mSlots.emplace_back( aFunction ); }
    void emit( TArgs... tArgs ) { for ( const auto& slot : mSlots) slot( tArgs... ); }

private:
    std::vector<TypeFunction> mSlots{};
};

TEST( DesingPattern, signals )
{
    Signal<std::string> signal;
    auto printerOrdinary    = []( std::string s ) { std::cout << "Ordinary:    " << s << std::endl; };
    auto printerMagnificent = []( std::string s ) { std::cout << "Magnificent: " << s << std::endl; };

    signal.connect( printerOrdinary    );
    signal.connect( printerMagnificent );
    signal.connect( printerOrdinary    );
    signal.emit( "dupa print" );
}

TEST( DesingPattern, singleton )
{
    struct Singleton
    {
        static Singleton& GetInstance()
        {
            static Singleton singleton;
            return singleton;
        }

        std::string toString() 
        {
            static int numberOfCalls = 0; 
            return "Singleton as string representation. " + std::to_string( numberOfCalls++ ); 
        }

    private:
        explicit Singleton() = default;
    };

    std::cout << Singleton::GetInstance().toString() << std::endl;
    std::cout << Singleton::GetInstance().toString() << std::endl;
}

TEST( DesingPattern, strategy )
{
    struct Beverage
    {
        Beverage( std::function<int()> amountWaterMl, std::function<void()> brew )
            : mAmountWaterMl( amountWaterMl )
            , mBrew( brew ) {}

        void prepare()
        {
            mAmountWaterMl();
            mBrew();
        }

    private:
        std::function<int()>  mAmountWaterMl;
        std::function<void()> mBrew;
    };

    std::function<int()> amountMilkMl = []()
    {
        std::cout << "amountMilkMl\n";
        return 5;
    };

    Beverage beverage( amountMilkMl, []{ std::cout << "brew\n"; } );
    beverage.prepare();
}

TEST( DesingPattern, chain )
{
    auto executeWithNextStep = []( auto aCall, auto aNext )
    {
        if ( aNext ) return aCall() + aNext();
        return aCall();
    };

    auto getMilk      = [](){ return std::string{ "Milk "}; };
    auto getChocolate = [](){ return std::string{ "Chocolate "}; };
    auto getIce       = [](){ return std::string{ "Ice "}; }; 

    std::function<std::string()> prepareOption1 = std::bind( executeWithNextStep, getMilk, getChocolate );
    prepareOption1                              = std::bind( executeWithNextStep, getIce,  prepareOption1 );
    prepareOption1                              = std::bind( executeWithNextStep, getIce,  prepareOption1 );
    prepareOption1                              = std::bind( executeWithNextStep, getIce,  prepareOption1 );
    std::cout << prepareOption1() << std::endl;
}

template <typename ...TArgs>
struct ObserverWorld
{
    using TObserver = std::function< void( TArgs... ) >;
    void addObserver( const TObserver& aObserver ) { mObservers.push_back( aObserver ); }
    void propagate( TArgs... aArgs ) { for( auto& observer : mObservers ) observer( aArgs... ); };

    std::vector<TObserver> mObservers;
};

TEST( DesingPattern, observer )
{
    ObserverWorld<std::string> observerWorld;
    observerWorld.addObserver( []( std::string aOrder ){ std::cout << "Machine: preparing " << aOrder << std::endl; } );
    observerWorld.addObserver( []( std::string aOrder ){ std::cout << "Order's history:   " << aOrder << std::endl; } );
    observerWorld.addObserver( []( std::string aOrder ){ std::cout << "Update screen:     " << aOrder << std::endl; } );
    observerWorld.propagate( "Hot Chocolate" );
}

TEST( DesingPattern, factory )
{
    struct Beverage
    { 
        Beverage( const std::string& aName, const float aAmountMilk)
            : mName{ aName }
            , mAmountMilk{ aAmountMilk } {}
        
        void prepare() { std::cout << mName << mAmountMilk << std::endl; } 
        
        std::string mName;
        float mAmountMilk;
    };

    struct BeverageAbstractFactory
    {
        BeverageAbstractFactory()
        {
            mFactories["Tea"]    = [](){ return std::make_unique<Beverage>( "Tea:    ", 10.0f ); };
            mFactories["Coffee"] = [](){ return std::make_unique<Beverage>( "Coffee: ", 50.0f ); };
        }

        std::unique_ptr<Beverage> create( std::string aBeverageName )
        {
            std::unique_ptr<Beverage> returnValue = nullptr;

            auto iter = mFactories.find( aBeverageName );
            if ( mFactories.end() != iter ) returnValue = iter->second();

            return returnValue;
        }

        std::map< std::string, std::function< std::unique_ptr<Beverage>() > > mFactories;
    };

    BeverageAbstractFactory beverageFactory{};
    auto tea      = beverageFactory.create("Tea");
    auto coffee   = beverageFactory.create("Coffee");
    auto mismatch = beverageFactory.create( "mismatch" );
    if ( tea )       tea->prepare();
    if ( coffee )    coffee->prepare();
    if ( !mismatch ) std::cout << "Factory not found\n";
}

TEST( DesingPattern, decorator )
{
    struct WindowElement
    {
        virtual std::string toString() const = 0;
    };

    struct PushButton : public WindowElement
    {
        PushButton( std::string aName ) : mName( aName ){}
        std::string toString() const override { return "|PushButton " + mName + "|"; }

        std::string mName;
    };

    struct HorizontalScroll : public WindowElement
    {
        HorizontalScroll( std::string aName ) : mName( aName ){}
        std::string toString() const override { return "|HorizontalScroll " + mName + "|"; }

        std::string mName;
    };

    struct Window : public WindowElement
    {
        using WindowElements = std::vector<std::unique_ptr<WindowElement>>;

        void addElement( std::unique_ptr<WindowElement> aElement ){ mWindowElements.emplace_back( std::move( aElement ) ); }

        std::string toString() const override
        {
            std::string description = "Window: ";
            for ( const auto& element : mWindowElements ) description += element->toString();
            return description;
        }

        WindowElements mWindowElements;
    };

    Window window{};
    window.addElement( std::make_unique<PushButton>( "Button0" ) );
    window.addElement( std::make_unique<PushButton>( "Button1" ) );
    window.addElement( std::make_unique<HorizontalScroll>( "Scroll0" ) );
    window.addElement( std::make_unique<PushButton>( "Button2" ) );

    std::cout << window.toString() << std::endl;
}

TEST( DesingPattern, builder )
{
    class Person
    {
        friend class PersonBuilder;
    public:
        std::string toString()
        {
            std::string stringRepresentation = "";
            stringRepresentation += "Address: " + mStreetAddress + " "
                                                + mPostCode      + " "
                                                + mCity          + "\n";

            return stringRepresentation;
        }

    public:
        std::string mStreetAddress, mPostCode, mCity;
        uint32_t    mAnnualIncome;
    };

    class PersonBuilder
    {
    public:
        PersonBuilder( Person& person ) : mPerson{ person } {}

        PersonBuilder setStreetAddress( std::string address )
        {
            mPerson.mStreetAddress = address;
            return *this;
        }

        PersonBuilder setPostCode( std::string postCode) 
        {
            mPerson.mPostCode = postCode;
            return *this;
        }

        PersonBuilder setCity( std::string city )
        {
            mPerson.mCity = city;
            return *this;
        }

    protected:
        Person& mPerson;
    };

    Person person;
    PersonBuilder personBuilder{person};
    personBuilder.setCity("Wroclaw")
                 .setPostCode("eee post code")
                 .setStreetAddress("Legnicka");

    std::cout << person.toString() << std::endl;
}

TEST( DesingPattern, composite )
{
    struct Creature
    {
        enum Ability {strenght, agility, intelligence, count};  // These are the core of composite approach.
                                                                // Notice that count must be at the end. 
        Creature(){ abilities.fill( 0 ); }

        void set( const Ability aAbility, const int32_t aValue ) { abilities[aAbility] = aValue; }
        int32_t get( const Ability aAbility ) const { return abilities[aAbility]; }
        int32_t getSum() const { return std::accumulate( abilities.begin(), abilities.end(), 0 ); }

        std::array<int32_t, count> abilities;
    };

    Creature creature;
    creature.set( Creature::strenght,     100 );
    creature.set( Creature::agility,      110 );
    creature.set( Creature::intelligence, 120 );

    std::cout << "Average value of traits: " << creature.getSum() << std::endl;
}

TEST( DesingPattern, flyweight )
{
    struct TextRange
    {
        uint32_t start, end;
        bool covers( uint32_t position ) const { return position >= start && position <= end; }
    };

    class Formatter
    {
    public:
        Formatter(const TextRange&  textRange) : mTextRange{textRange} {}
        virtual void format(char& letter, uint32_t position) const = 0;

    protected:
        TextRange mTextRange;
    };

    struct ToUpper : public Formatter
    {
        ToUpper(const TextRange&  textRange) : Formatter{ textRange } {}
        void format( char& letter, uint32_t position ) const override
        {
            if ( mTextRange.covers( position ) ) letter = std::toupper( letter );
        }
    };

    struct ToLower : public Formatter
    {
        ToLower(const TextRange&  textRange) : Formatter{textRange} {}
        void format(char& letter, uint32_t position) const override
        {
            if ( mTextRange.covers( position ) ) letter = std::tolower( letter );
        }
    };

    class BetterFormattedText
    {
    public:
        BetterFormattedText(std::string& text) :  mPlainText{text} {}
        BetterFormattedText(std::string&& text) :  mPlainText{text} {}

        void addFormatter( std::unique_ptr<Formatter>&& formatter ){ mFormatters.emplace_back( std::move( formatter ) ); }

        std::string toString()
        {
            std::string returnValue = mPlainText;

            for ( uint32_t i = 0; returnValue.length() > i; ++i )
            {
                for ( const auto& formatter : mFormatters ) formatter->format( returnValue[i], i );
            }

            return returnValue;
        }

    private:
        std::string mPlainText;
        std::vector<std::unique_ptr<Formatter>> mFormatters;
    };

    BetterFormattedText formattedText{ "TeXtForFoRmaTtInG" };
    formattedText.addFormatter( std::make_unique<ToLower>( TextRange{ 0,  3 } ) );
    formattedText.addFormatter( std::make_unique<ToUpper>( TextRange{ 4,  6 } ) );
    formattedText.addFormatter( std::make_unique<ToLower>( TextRange{ 7, 16 } ) );
    std::cout << formattedText.toString() << std::endl;
}
