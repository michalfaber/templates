#include <iostream>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <functional>
#include <cstdarg>
#include <memory>
#include <vector>
#include <random>
#include <numeric>
#include <locale>
#include <gtest/gtest.h>
#include "../common/Figure.hpp"

std::string get_middle( const std::string& input )
{
    if (input.length() % 2 == 0) {
        return input.substr(input.length() / 2 - 1, 2);
    } else {
        return input.substr(input.length() / 2, 1);
    }
}

/// Returning pointer from function. Examples of bad practise

std::unique_ptr<uint32_t> getByValue(uint32_t value)
{
    return std::make_unique<uint32_t>(value);
}


/// Parameter pack and Variadic functions

// Notice the size of output elf file
// Compare the size in case when program uses
// AggregateStringsInVectorBad vs AggregateStringsInVectorBetter
std::vector<std::string> AggregateStringsInVectorBad(){ return {}; }

template <typename FirstArg, typename ... Args>
std::vector<std::string> AggregateStringsInVectorBad(const FirstArg &firstArg,
                                                     const Args &... arg)
{
    const auto getStringFromArg = [&](const auto & t) -> std::string
    {
        std::stringstream ss;
        ss << t;
        return ss.str();
    };

    std::vector<std::string> vectorStrings;
    vectorStrings.push_back(getStringFromArg(firstArg));

    auto remainder = AggregateStringsInVectorBad(arg...);
    vectorStrings.insert(vectorStrings.end(), remainder.begin(), remainder.end());

    return vectorStrings;
};

template <typename ... Args>
std::vector<std::string> AggregateStringsInVectorBetter(const Args&... arg)
{
    const auto getStringFromArg = [&](const auto & t) -> std::string
    {
        std::stringstream ss;
        ss << t;
        return ss.str();
    };

    return { getStringFromArg(arg)... };
};

std::vector<std::string> AggregateStringsInVectorOldStyle(const char* fmt, ...)
{

    const auto getStringFromArg = [&](const auto & t) -> std::string
    {
        std::stringstream ss;
        ss << t;
        return ss.str();
    };

    std::vector<std::string> returnAggregatedStrings{};

    va_list args;
    va_start(args, fmt);

    while (*fmt != '\0')
    {
        switch (*fmt)
        {
        case 'd':
        {
            returnAggregatedStrings.emplace_back(std::to_string((va_arg(args, uint32_t))));
            break;
        }
        case 's':
        {
            returnAggregatedStrings.emplace_back(std::string((va_arg(args, char*))));
            break;
        }
        default: break;
        }
        ++fmt;
    }

    va_end(args);

    return returnAggregatedStrings;
}


/// Perfect forwarding

decltype(auto) FunctionAutoTypeDeduction(){ return std::string{"Automatically deduced type!"}; }

template <class T>
using my_remove_reference_t = typename std::remove_reference<T>::type;

void Overloaded (const uint32_t& arg) { std::cout << __PRETTY_FUNCTION__ << std::endl; }
void Overloaded (uint32_t&& arg)      { std::cout << __PRETTY_FUNCTION__ << std::endl; }

template< typename T >
void forwarding (T && arg)
{
    std::cout << "by simple passing: ";
    Overloaded(arg);

    std::cout << "via std::forward: ";
    Overloaded(std::forward<T>(arg));

    std::cout << "via std::move: ";
    Overloaded(std::move(arg));         // conceptually this would invalidate arg
}

TEST( Function, autoTypeDeduction )
{
    std::cout << FunctionAutoTypeDeduction();

    std::cout << "initial caller passes rvalue:\n";
    forwarding( 5 );
    std::cout << "\ninitial caller passes lvalue:\n";
    uint32_t xInt32_t = 5;
    forwarding( xInt32_t );
}

TEST( Lambda, captureByRefAndValue )
{
    uint32_t valueToEditInLambda  = 6;
    uint32_t valueToPassByCopping = 123;
    // Notice mutable, without it editing z is not possible
    auto functionX = [&valueToEditInLambda, valueToPassByCopping]() mutable -> uint32_t {
        valueToEditInLambda *= 2;
        return valueToPassByCopping;
    };
    std::cout << " return value from functionX: " << functionX()
                << " valueToEditInLambda: " << valueToEditInLambda
                << std::endl;
}

TEST( Lambda, nestedLambdaFunctions )
{
    std::function < uint32_t( uint32_t, uint32_t ) > functionTry 
        = []( uint32_t a, uint32_t b ) { return a * b; };
    std::cout << "functionTry: " << functionTry(2, 3) << std::endl;

    uint32_t timesTwoPlusThree = []( uint32_t x )
    {
        return []( uint32_t y )      //definition of another lambda function.
        {
            return y * 2;
        }( x ) + 3;                  //calling defined function with argument x.
    }( 12 );
    std::cout << "timesTwoPlusThree: " << timesTwoPlusThree << std::endl;
}

TEST( Lambda, returnOtherLambdaDefiniction )
{
    auto addTwoIntegers = [](uint32_t x) -> std::function < uint32_t(uint32_t) >
    {
        //returns function which is calling with int parameter and returning int value.
        return[=](uint32_t y) { return x + y; };
    };// function maker

    auto higherOrder = [](const std::function<int(int)> &f, int z) {
        //returns int value after calling with argument of function<int(int)>
        return f(z) * 2;
    };
    auto answer = higherOrder(addTwoIntegers(7), 8);
    std::cout << "higherOrder: " << answer << std::endl;
}

TEST( RandomNumbers, generatorEngine )
{
    std::vector<uint32_t> v = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    std::random_device rd;
    std::mt19937_64 g(rd());

    std::shuffle(v.begin(), v.end(), g);

    std::copy(v.begin(), v.end(), std::ostream_iterator<uint32_t>(std::cout, " "));
    std::cout << "\n";
}

TEST( String, getMiddle )
{
    std::cout << get_middle("sdef81") << std::endl;
}

TEST( ReturnSmartPointer, badPractise )
{
    auto& value = *getByValue(5);
    std::cout << *getByValue(7) << std::endl;
    std::cout << value << std::endl;  // ups, something went wrong :)
}

TEST( MemoryAlignment, badAndGoodExample )
{
    struct Badly
    {
        uint64_t byte_8;
        uint8_t  byte_1;
        uint32_t byte_4;
        uint8_t  byte_1_;
    };

    struct Better
    {
        uint64_t byte_8;
        uint32_t byte_4;
        uint8_t  byte_1;
        uint8_t  byte_1_;
    };

    std::cout << "Badly: "  << sizeof(Badly) << std::endl;
    std::cout << "Better: " << sizeof(Better) << std::endl;

    Badly aBadly[10];
    Better aBetter[10];
    std::cout << "aBadly: "  << sizeof(aBadly) << std::endl;
    std::cout << "aBetter: " << sizeof(aBetter) << std::endl;


    struct BadlyWithVirtualMethod
    {
        Badly badly;     // probably 24bytes, only if there was NO change in struct Badly
        virtual void doSth() = 0;
    };

    // Note that the size is bigger than object of struct Badly. vTable.
    std::cout << "BadlyWithVirtualMethod: "  << sizeof(BadlyWithVirtualMethod)  << std::endl;
}

TEST( VariadicParameterList, parameterPackAndVariadicFunction )
{
    // const auto vec = AggregateStringsInVectorBad("ass ", 123, " ", 33, " hehh ");
    const auto vec = AggregateStringsInVectorBetter("ass ", 123, " ", 33, " hehh ");
    for (const auto& v : vec) std::cout << v;
    std::cout << std::endl;

    const auto vecOldStyle = AggregateStringsInVectorOldStyle("sdsds"/* s - string, d - digits */,
                                                                "ass ", 123, " ", 33, " hehh ");
    for (const auto& v : vecOldStyle) std::cout << v;
    std::cout << std::endl;
}

TEST( Variable, constReference )
{
    uint32_t intValue = 6;
    const uint32_t& constRefInt = intValue;
    std::cout << "constRefInt: " << constRefInt << std::endl;
}

TEST( Tuple, getAndTie )
{
    auto myTuple = std::make_tuple<uint32_t, std::string, double>(5, "Just string", 1.11);
    std::cout << std::get<0>(myTuple) << " " << std::get<1>(myTuple) << " " << std::get<2>(myTuple) << std::endl;

    std::string justString;
    double value;
    std::tie(std::ignore, justString, value) = myTuple;
    std::cout << justString << " " << value << std::endl;
}

TEST( Locale, currency )
{
    std::locale loc = std::locale(""); // user's preferred locale
    std::cout << "Your currency string is "
              << std::use_facet< std::moneypunct< char, true > >( loc ).curr_symbol() 
              << '\n';
}

TEST( Sizeof, differentTypes )
{
    char     a[] = "Hello";
    wchar_t  b[] = L"Hello";
    char16_t c[] = u"Hello";
    char32_t d[] = U"Hello";

    std::cout << "char:     " << sizeof( a ) << std::endl
              << "wchar_t:  " << sizeof( b ) << std::endl
              << "char16_t: " << sizeof( c ) << std::endl
              << "char32_t: " << sizeof( d ) << std::endl;
}
