#include "app_multithreading/component/process_data.h"
#include "app_multithreading/message/propagator.h"
#include "app_multithreading/parallel/executor.h"
#include <future>
#include <gtest/gtest.h>

TEST( MessagePropagator, propagator )
{
    message::Propagator<int> messagePropagator;

    auto pipe0 = messagePropagator.createPipe( );
    auto pipe1 = messagePropagator.createPipe( );

    messagePropagator.addMessage( std::make_unique<int>( 33 ) );
    messagePropagator.propagate( );
    std::cout << "pipe0: " << *pipe0->getMessage( ) << std::endl;
    std::cout << "pipe1: " << *pipe1->getMessage( ) << std::endl;
}

TEST( ParallelExecution, nullptrWork )
{
    EXPECT_ANY_THROW( parallel::Executor parallelExecution( nullptr ) );
}

TEST( ParallelExecution, componentProcessData )
{
    using namespace std::chrono;

    parallel::Executor parallelExecution(
        [aProcessData = component::ProcessData {}]( ) mutable { aProcessData.work( ); } );
    parallelExecution.startExecution( );

    std::this_thread::sleep_for( 1s );

    parallelExecution.endExecution( );
    parallelExecution.startExecution( );

    parallelExecution.endExecution( );
    parallelExecution.startExecution( );

    std::this_thread::sleep_for( 1s );
}

TEST( ParallelExecution, messagePropagator )
{
    using namespace std::chrono;

    auto               propagator = std::make_shared<message::Propagator<std::string>>( );
    parallel::Executor parallelExecution(
        [aPropagator = propagator]( ) { aPropagator->propagateBlocking( ); } );
    parallelExecution.startExecution( );

    auto pipe    = propagator->createPipe( );
    auto thread0 = std::async( std::launch::async, [pipe]( ) {
        uint32_t receivedMessages = 0;
        while ( receivedMessages < 5 )
        {
            auto message = pipe->getMessageBlocking( 100ms );
            if ( message )
            {
                ++receivedMessages;
            }
            std::cout << "Received: " + ( message ? *message : "nothing" ) << std::endl;
        }
    } );

    auto thread1 = std::async( std::launch::async, [propagator]( ) {
        for ( uint32_t i = 0; i < 5; ++i )
        {
            propagator->addMessage(
                std::make_unique<std::string>( "message " + std::to_string( i ) ) );
        }
    } );
}

#include "app_multithreading/component/screen.h"
TEST( Screen, messagePropagator )
{
    using namespace std::chrono;

    auto propagatorLoopEnter = std::make_shared<message::Propagator<bool>>( );
    auto propagatorLoopExit  = std::make_shared<message::Propagator<bool>>( );
    auto propagatorGate      = std::make_shared<message::Propagator<bool>>( );

    ;
    parallel::Executor parallelScreen(
        [screen
         = component::Screen( propagatorLoopEnter->createPipe( ), propagatorLoopExit->createPipe( ),
                              propagatorGate->createPipe( ) )]( ) mutable { screen.update( ); } );
    parallelScreen.startExecution( );

    parallel::Executor parallelPropagatorLoopEnter(
        [aPropagator = propagatorLoopEnter]( ) { aPropagator->propagateBlocking( ); } );
    parallelPropagatorLoopEnter.startExecution( );

    parallel::Executor parallelPropagatorLoopExit(
        [aPropagator = propagatorLoopExit]( ) { aPropagator->propagateBlocking( ); } );
    parallelPropagatorLoopExit.startExecution( );

    parallel::Executor parallelPropagatorGate(
        [aPropagator = propagatorGate]( ) { aPropagator->propagateBlocking( ); } );
    parallelPropagatorGate.startExecution( );

    auto thread0 = std::async( std::launch::async, [propagatorLoopEnter]( ) {
        bool beginState = false;
        for ( int i = 0; i < 4; ++i )
        {
            propagatorLoopEnter->addMessage( std::make_unique<bool>( beginState ) );
            beginState = ! beginState;
            std::this_thread::sleep_for( std::chrono::seconds {1} );
        }
    } );

    auto thread1 = std::async( std::launch::async, [propagatorLoopExit]( ) {
        bool beginState = false;
        for ( int i = 0; i < 4; ++i )
        {
            propagatorLoopExit->addMessage( std::make_unique<bool>( beginState ) );
            beginState = ! beginState;
            std::this_thread::sleep_for( std::chrono::seconds {1} );
        }
    } );

    auto thread2 = std::async( std::launch::async, [propagatorGate]( ) {
        bool beginState = false;
        for ( int i = 0; i < 12; ++i )
        {
            propagatorGate->addMessage( std::make_unique<bool>( beginState ) );
            beginState = ! beginState;
            std::this_thread::sleep_for( std::chrono::milliseconds {200} );
        }
    } );
}

#include "app_multithreading/component/gate.h"
#include "app_multithreading/component/loop_enter.h"
#include "app_multithreading/component/loop_exit.h"
TEST( Screen, communicationComponents )
{
    using namespace std::chrono;

    auto               propagatorLoopEnter = std::make_shared<message::Propagator<bool>>( );
    parallel::Executor parallelPropagatorLoopEnter(
        [aPropagator = propagatorLoopEnter]( ) { aPropagator->propagateBlocking( ); } );
    parallelPropagatorLoopEnter.startExecution( );

    auto               propagatorLoopExit = std::make_shared<message::Propagator<bool>>( );
    parallel::Executor parallelPropagatorLoopExit(
        [aPropagator = propagatorLoopExit]( ) { aPropagator->propagateBlocking( ); } );
    parallelPropagatorLoopExit.startExecution( );

    auto               propagatorGate = std::make_shared<message::Propagator<bool>>( );
    parallel::Executor parallelPropagatorGate(
        [aPropagator = propagatorGate]( ) { aPropagator->propagateBlocking( ); } );
    parallelPropagatorGate.startExecution( );

    parallel::Executor parallelScreen(
        [screen
         = component::Screen( propagatorLoopEnter->createPipe( ), propagatorLoopExit->createPipe( ),
                              propagatorGate->createPipe( ) )]( ) mutable { screen.update( ); } );
    parallelScreen.startExecution( );

    parallel::Executor parallelLoopEnter(
        [aLoopEnter
         = component::LoopEnter( propagatorLoopEnter )]( ) mutable { aLoopEnter.update( ); },
        std::chrono::milliseconds {200} );
    parallelLoopEnter.startExecution( );

    parallel::Executor parallelLoopExit(
        [aLoopExit = component::LoopExit( propagatorLoopExit )]( ) mutable { aLoopExit.update( ); },
        std::chrono::milliseconds {200} );
    parallelLoopExit.startExecution( );

    parallel::Executor parallelGate(
        [aGate = component::Gate( propagatorGate )]( ) mutable { aGate.update( ); },
        std::chrono::milliseconds {300} );
    parallelGate.startExecution( );

    std::this_thread::sleep_for( std::chrono::seconds {3} );
}
