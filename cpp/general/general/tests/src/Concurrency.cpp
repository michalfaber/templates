
#include "include/Helpers.hpp"
#include <gtest/gtest.h>

#include <string>

#include <vector>
#include <memory>
#include <exception>

// Warning: A few tests crash during runtime. This was done on purpuse.

TEST( std_thread__start, error )
{
    /*
     * Goal: Show problem caused by lack of std::thread::join.
     *
     * Description: Apparently, a problem with giving a birth to a new thread but
     * exactly, it is a problem with destruction of joinable std::thread obj.
    */

    std::thread thread( Function, 666);

    // ups! Lack of thread.join(). It is wrong behaviour to destruct joinable std::thread obj and
    // it results in std::terminate call.
}

TEST( std_thread__end, error_fixedByJoining )
{
    /*
     * Goal: Show solution to a problem cause by lack of std::thread::join just by adding one.
    */

    std::thread thread( Function, 123);
    thread.join();							// Let's try with join
}

TEST( std_thread__end, error_fixedByDetaching )
{
    /*
     * Goal: Show solution to a problem caused by lack of std::thread::join just by detaching the thread from obj.
    */

    std::thread thread( Function, 666 );
    thread.detach();						// Let's detach the thread from std::thread obj.

    /*
     * But where is our print? Was the Function even executed?
     * There is a chance to end whole process before the thread will even start.
     * In OS usually end of a process equals to end of all threads under the process scope ( cleaning resources gathered by a process ).
     * That's why sometimes there will be a print with 666 value and sometime will not.
    */
}

TEST( std_thread__end, error_joinDetached )
{
    /*
     * Goal: Show problem cause by std::thread::join call on detached std::thread obj.
    */

    std::thread thread( Function, 666 );
    thread.detach();						// Let's detach the thread from std::thread obj.

    thread.join();							// Because why not	:D
}

TEST( std_thread__end, error_fixedByJoiningDetachedWithHack )
{
    /*
     * Goal: Show how to join a deatached thread using std::promise and std::future.
    */

    auto FunctionWithSync = []( uint32_t a_iArg, std::promise<void>&& a_promise )
    {
        std::cout << __PRETTY_FUNCTION__ << ": " << a_iArg << std::endl;
        a_promise.set_value_at_thread_exit();								// Signaling end of execution to future obj
    };

    std::promise<void> promise;
    std::future future = promise.get_future();

    std::thread thread( FunctionWithSync, 123, std::move( promise ) );		// Notice that promise obj is moved.
    thread.detach();														// Detaching...

    future.get();															// Blocking operation until std::promise::set_value is called, "artificially" joining the thread.

    // Notice: explicit std::thread::join call is not needed when std::thread obj lost joinbale status (is not joinable at the moment of std::thread obj destruction).
}

TEST( std_thread__start, waysOfStarting )
{
    /*
     * Goal: Show how to start a thread with passing diffrent "types of callable".
    */

    std::vector<std::thread> threads;
    threads.emplace_back( std::thread( Function, 0) );
    Callable callable;
    threads.emplace_back( std::thread( &Callable::Method, &callable, 1 ) );
    threads.emplace_back( []( uint32_t a_iArg )
    {
        std::cout << __PRETTY_FUNCTION__ << ": " << a_iArg << std::endl;
    }, 2 );

    for ( auto& thread : threads )
    {
        if ( thread.joinable() ) thread.join();
    }

    // Question: Why is the output "sliced" soo much?
}

TEST( std_thread__start, waysOfStarting_printStringLessSliced )
{
    /*
     * Goal: Show a way to less "sliced" output of stream.
    */

    std::vector<std::thread> threads;
    threads.emplace_back( std::thread( FunctionWholeString, 0) );
    Callable callable;
    threads.emplace_back( std::thread( &Callable::MethodWholeString, &callable, 1 ) );
    threads.emplace_back( []( uint32_t a_iArg )
    {
        std::cout << std::string( __PRETTY_FUNCTION__ ) + " " + std::to_string( a_iArg ) + "\n";
    }, 2 );

    for ( auto& thread : threads )
    {
        if ( thread.joinable() ) thread.join();
    }

    // Writing to stream a string as a whole one should help a little bit.
    // Notice: nondeterministic behaviour of prints ( other order than 0, 1, 2 ).
}

TEST( std_thread__returnValue, sharePointer )
{
    /*
     * Goal: Show how to return value from std::thread obj with smart pointer.
    */

    auto pData = std::make_shared<uint32_t>( 0 );
    auto Function = []( std::shared_ptr<uint32_t> a_pData )		// Passing by value is the safer way. Allows to avoid problems with collapsed area.
    {
        *a_pData += 1;
    };

    std::vector<std::thread> threads;
    constexpr uint32_t numberOfThreads = 1;
    for ( uint32_t i = 0; i < numberOfThreads; ++i )
    {
        threads.emplace_back( Function, pData );
    }

    for ( auto& thread : threads )
    {
        if ( thread.joinable() ) thread.join();
    }
    std::cout << "Processed data by " << numberOfThreads << " threads: " << *pData << std::endl;

    // Question: Is the precisely current implementation without critical section problems?
    //			 What if the std::cout is placed before std::thread::join?
    //			 What if the numberOfThreads is increased?
}

TEST( std_thread__returnValue, std_promise )
{
    /*
     * Goal: Show how to return value from std::thread obj with std::promise and std::future
    */

    auto Function = []( std::promise<int>&& a_promise ){ a_promise.set_value( 1 ); };

    std::promise<int> promise;								// Notice that return type <int> is specified here.
    auto future = promise.get_future();						// future for later std::future::get() call.
    std::thread thread( Function, std::move( promise ) );	// Start thread and move std::promise obj.

    // Wait blocks until std::promise::set_value is called and maybe be performed many times...
    future.wait();
    future.wait();

    // Get also is blocking operation until std::promise::set_value is called.
    std::cout << "return value: " << future.get() << std::endl;

    // To be sure that thread has ended his execution before going out of the current scope.
    if ( thread.joinable() )
    {
        if ( thread.joinable() ) thread.join();
    }
}

TEST( std_thread__returnValue, std_promise_error_waitAfterGet )
{
    /*
     * Goal: Show that std::future::wait call exectued after std::future::get results in crash.
    */

    auto Function = []( std::promise<int>&& a_promise ){ a_promise.set_value( 1 ); };

    std::promise<int> promise;								// Notice that return type <int> is specified here.
    auto future = promise.get_future();						// future for later std::future::get() call.
    std::thread thread( Function, std::move( promise ) );

    // Get is blocking operation until std::promise::set_value is called.
    std::cout << "return value: " << future.get() << std::endl;
    future.wait();		// ups, std::future::wait can't be called if std::future::get has been already executed
    // so let's crash...

    // To be sure that thread has ended his execution before going out of the current scope.
    if ( thread.joinable() )
    {
        if ( thread.joinable() ) thread.join();
    }
}

TEST( std_thread__returnValue, std_promise_error_tooManyGets )
{
    /*
     * Goal: Show that only one std::future::get call is allowed.
    */

    auto Function = []( std::promise<int>&& a_promise ){ a_promise.set_value( 1 ); };

    std::promise<int> promise;								// Notice that return type <int> is specified here.
    auto future = promise.get_future();						// future for later std::future::get() call.
    std::thread thread( Function, std::move( promise ) );

    // Get is blocking operation until std::promise::set_value is called.
    std::cout << "return value: " << future.get() << std::endl;
    std::cout << "return value: " << future.get() << std::endl;		// ups, std::future::get must be called only once
                                                                    // so let's crash...

    // To be sure that thread has ended his execution before going out of the current scope.
    if ( thread.joinable() )
    {
        if ( thread.joinable() ) thread.join();
    }
}

TEST( std_thread__returnValue, std_promise_error_noNeedForJoin )
{
    /*
     * Goal: Show that even a finished thread needs std::thread::join call
    */

    auto Function = []( std::promise<int>&& a_promise ){ a_promise.set_value_at_thread_exit( 1 ); };	// Set value at a thread's end.

    std::promise<int> promise;								// Notice that return type <int> is specified here.
    auto future = promise.get_future();						// future for later std::future::get() call.
    std::thread thread( Function, std::move( promise ) );

    // Get is blocking operation until std::promise::set_value is called.
    auto returnValue = future.get();
    std::cout << "return value: " << returnValue << std::endl;

    //if ( thread.joinable() ) thread.join();
    // ups, crash. Yes, join is still needed.
}

TEST( std_thread__returnValue, std_promise_error_forgetToSetValue )
{
    /*
     * Goal: Show that std::future::get call results in cash when there will never be a std::promise::set_value call.
    */

    auto Function = []( std::promise<int>&& a_promise ) { /* a_promise.set_value( 1 ); */ };	// A forgotten std::promise::set_value call

    std::promise<int> promise;								// Notice that return type <int> is specified here.
    auto future = promise.get_future();						// future for later std::future::get() call.
    std::thread thread( Function, std::move( promise ) );

    // Get is blocking operation until std::promise::set_value is called.
    std::cout << "return value: " << future.get() << std::endl;		// ups, lack of std::promise::set_value
    // so let's crash
    if ( thread.joinable() )
    {
        if ( thread.joinable() ) thread.join();
    }
}

TEST( std_thread__returnValue, std_promise_forgetToGet )
{
    /*
     * Goal: Show that forgotten std::future::get call is not a problem.
    */

    auto Function = []( std::promise<int>&& a_promise ) {  a_promise.set_value( 1 ); };

    std::promise<int> promise;								// Notice that return type <int> is specified here.
    auto future = promise.get_future();						// future for later std::future::get() call.
    std::thread thread( Function, std::move( promise ) );

    // Get is blocking operation until std::promise::set_value is called.
    //auto returnValue = future.get();
    //std::cout << "return value: " << returnValue << std::endl;

    if ( thread.joinable() )
    {
        if ( thread.joinable() ) thread.join();
    }
    // No problem	:)
}

TEST( std_thread__returnValue, std_packaged_task )
{
    /*
     * Goal: Show how to return value from std::thread obj with std::packaged_task
    */

    std::packaged_task<int( int, int )> task( []( int a, int b ) { return a + b; } );
    auto future = task.get_future();		// Future has to be taken before starting task in new thread.
    // Otherwise, there is a crash during thread execution.
    // And get_future() must be called only once.

    // task( 1, 2 ); this one doesn’t start execution of the task in a separate thread.
    // Explicit thread creation is needed to start the task in a seperate thread.
    std::thread thread( std::move( task ), 1 , 2 );

    // std::promise::set_value is executed implicitly at return from std::package_task function.
    std::cout << "return value: " << future.get() << std::endl;

    if ( thread.joinable() ) // Let’s don’t forget about the thread.
    {
        if ( thread.joinable() ) thread.join();
    }
}

TEST( std_thread__returnValue, std_packaged_task_error_tooManyCallsTask )
{
    /*
     * Goal: Show that there should be only one call of a function from with std::packaged_task.
    */

    std::packaged_task<int( int, int )> task( []( int a, int b ) { return a + b; } );
    auto future = task.get_future();		// Future has to be taken before starting task in new thread.
    // Otherwise, there is a crash during thread execution.
    // And get_future() must be called only once.

    task( 1, 2 );										// first call of task...
    std::thread thread( std::move( task ), 1 , 2 );		// ... second call of the same task.
    // ups, let's crash...

    // std::promise::set_value is executed implicitly at return from std::package_task function.
    std::cout << "return value: " << future.get() << std::endl;

    if ( thread.joinable() ) // Let’s don’t forget about the thread.
    {
        if ( thread.joinable() ) thread.join();
    }
}

TEST( std_async__start, waysOfStarting )
{
    /*
     * Goal: Show how to start a thread with passing diffrent "types of callable".
    */

    std::future future0( std::async( std::launch::async, Function, 0 ) );
    Callable callable;
    std::future future1( std::async( std::launch::async, &Callable::Method, &callable, 1.0f ) );
    std::future future2( std::async( std::launch::async, []( uint32_t a_iArg )
    {
                             std::cout << std::string( __PRETTY_FUNCTION__ ) + " " + std::to_string( a_iArg ) + "\n";
                         }, 2 ) );

    // No need for explicit get/join of std::future returned from std::async because
    // destruction of the std::future is blocing until a thread was finished.
}

TEST( std_async__end, letsPlay )
{
    /*
     * Goal: Show other nondeterministic behaviour and emphasise that
     *		 destruction of last std::future associated with std::async is considered as blocking operation.
    */

    // What's the order of prints?

    srand ( time( NULL ) );
    auto Function = []( uint32_t a_iArg )
    {
        // The very below ugly line only to randomise execution time.
        std::this_thread::sleep_for( std::chrono::milliseconds( static_cast<uint32_t>( std::rand() ) % ( a_iArg + 1) ) );
        std::cout << "Value: " + std::to_string( a_iArg ) + "\n";
    };

    const uint32_t numberOfThreads = 10;
    for ( uint32_t i = 0; i < numberOfThreads; ++i) std::async( std::launch::async, Function, i );

    std::future future( std::async( std::launch::async, Function, 666 ) );

    for ( uint32_t i = 0; i < numberOfThreads; ++i) std::async( std::launch::async, Function, i + 100 );

    // Question: Why is order of counts 0-9 and 100-109 sequential?
    //			 Why is 666 placed in random lines where 0-9 and 100-109 is sequential?
}

TEST( std_async__returnValue, std__future )
{
    /*
     * Goal: Show how get returned value from a thread created by std::async.
    */

    std::future future( std::async( std::launch::async, []( uint32_t a_iArg )
    {
                            return std::string( "a_iArg: ") + std::to_string(a_iArg);
                        }, 123 ) );

    std::cout << future.get() << std::endl;		// Easy one
}

TEST( exception, std_async )
{
    /*
     * Goal: Show how to retrive exception from a thread
    */
    auto FunctionWithException = []( )
    {
        throw "Sth went wrong";
    };
    std::future<void> future;

    try
    {
        future = std::async( std::launch::async, FunctionWithException );
        future.get(); // this line will propagate exceptions from inside of async thread!!!

    } catch ( const char* e)
    {
        std::cout << "Cought: " << e << std::endl;
    }
}

TEST( exception, std_thread )
{
    /*
     * Goal: Show how to retrive exception from a thread
    */

    try
    {
        auto FunctionWithException = []( )
        {
            try {
                throw "Sth went wrong";

            } catch ( ... ) {
                std::cout << "sth was caught inside std::thread" << std::endl;
//                throw; // this line will break application
            }
        };

        auto thread = std::thread( FunctionWithException );
        thread.join();

    } catch ( const char* e)
    {
        std::cout << "Cought: " << e << std::endl;
    }
}

TEST( synchronization, oneToOne )
{
    /*
     * Goal: Show sync method for an event
    */

    auto functionThread = []( std::future<void> a_future )
    {
        a_future.wait();
        std::cout << "Print from thread: " << std::this_thread::get_id() << std::endl;
    };

    std::promise<void> promise;
    auto future = promise.get_future();

    auto result0 = std::async( std::launch::async, functionThread, std::move( future ) );

    std::cout << "Print before weakuping a thread\n";
    promise.set_value();
}

TEST( synchronization, oneToManyWithData )
{
    /*
     * Goal: Show sync method for an event
    */

    std::vector<std::future<void>> futureThreads;

    std::promise<std::string> promise;
    std::shared_future<std::string> shared_future( promise.get_future() );	// std::shared_future so there is possibility to "block many threads until an event"

    // Notice passing shared_future by value(what’s not possible with std::future) and blocking get()
    auto functionThread = [shared_future]()
    {
        std::cout << std::this_thread::get_id() << " Received data" << shared_future.get() << std::endl;	// will wait until std::promise::set_value
    };

    const uint32_t numberOfThreads  = 8;
    for ( uint32_t i = 0; i < numberOfThreads; ++i) futureThreads.emplace_back( std::async( std::launch::async, functionThread ) );

    // Unblock threads and simultaneously send data to these threads.
    std::cout << "	Before taking off the barrier\n";
    promise.set_value( std::string( "Attention, let's back to work!" ) );
}

TEST( criticalSection, noProtection )
{
    /*
     * Goal: Show a nondeterministic behaviour of editing not protected shared resource.
    */

    // Not protected write to shared data obj.
    auto pData = std::make_shared<uint32_t>( 0 );
    std::vector<std::future<void>> futures;

    const uint32_t numberOfThreads  = 4;
    const uint32_t numberOfAddLoops = 1000;
    for ( uint32_t i = 0; i < numberOfThreads; ++i)
    {
        futures.emplace_back( std::async( std::launch::async, [pData]( )
        {
            for ( uint32_t i = 0; i < numberOfAddLoops; ++i )
            {
                // sleep prevents "nasty compiler" from optimalization and adds extra context switching
                std::this_thread::sleep_for( std::chrono::nanoseconds( 1 ) );
                *pData += 1;
            }
        } ) );
    }

    for ( auto& future : futures ) future.wait();

    EXPECT_EQ( *pData, numberOfThreads * numberOfAddLoops );
}

TEST( criticalSection, std_mutex__protection )
{
    /*
     * Goal: Show a protection for shared resource with std::mutex.
    */

    auto pData = std::make_shared<uint32_t>( 0 );	// Resource
    std::mutex mutexData;							// "Protection" for the above resource

    std::vector<std::future<void>> futures;
    const uint32_t numberOfThreads  = 4;
    const uint32_t numberOfAddLoops = 100;

    for ( uint32_t i = 0; i < numberOfThreads; ++i)
    {
        futures.emplace_back( std::async( std::launch::async, [pData, &mutexData]( )
        {
            for ( uint32_t i = 0; i < numberOfAddLoops; ++i )
            {
                // sleep slightly prevents "nasty compiler" from optimalization and adds extra context switching. Probably the context switch cause mostly what we want :)
                std::this_thread::sleep_for( std::chrono::nanoseconds( 1 ) );
                mutexData.lock();
                *pData += 1;
                mutexData.unlock();
            }
        } ) );
    }

    for ( auto& future : futures ) future.wait();

    EXPECT_EQ( *pData, numberOfThreads * numberOfAddLoops );
}

TEST( criticalSection, std_atomic__protection )
{
    /*
     * Goal: Show a protection for shared resource with std::atomic.
    */

    auto pData = std::make_shared<std::atomic_uint32_t>( 0 );	// Resource and "protection" in one step
    std::vector<std::future<void>> futures;

    const uint32_t numberOfThreads  = 4;
    const uint32_t numberOfAddLoops = 100;
    for ( uint32_t i = 0; i < numberOfThreads; ++i)
    {
        futures.emplace_back( std::async( std::launch::async, [pData]( )
        {
            for ( uint32_t i = 0; i < numberOfAddLoops; ++i )
            {
                // sleep slightly prevents "nasty compiler" from optimalization
                // and adds extra context switching. Probably the context switch causes mostly what we want here :)
                std::this_thread::sleep_for( std::chrono::nanoseconds( 1 ) );
                *pData += 1;
            }
        } ) );
    }

    for ( auto& future : futures ) future.wait();

    EXPECT_EQ( *pData, numberOfThreads * numberOfAddLoops );
}

TEST( criticalSection, timeComplexity_mutex_vs_atomic )		// TODO: simplyfy, threads are not needed
{
    /*
     * Goal: At least draw a littile bit the difference in time complexity between std::mutex and std::atomic
    */

    const uint32_t numberOfAddLoops = 1000000;

    std::mutex mutexData;
    uint64_t data = 0 ;
    std::chrono::steady_clock::time_point timePointBegin = std::chrono::steady_clock::now();

    for ( uint32_t i = 0; i < numberOfAddLoops; ++i )
    {
        mutexData.lock();
        data += 1;
        mutexData.unlock();
    }

    std::chrono::steady_clock::time_point timePointEnd = std::chrono::steady_clock::now();
    auto executionTimeMutex = std::chrono::duration_cast<std::chrono::duration<double>>( timePointEnd - timePointBegin );


    std::atomic_uint64_t atomicData = 0 ;
    timePointBegin= std::chrono::steady_clock::now();

    for ( uint32_t i = 0; i < numberOfAddLoops; ++i )
    {
        atomicData += 1;
    }

    timePointEnd = std::chrono::steady_clock::now();
    auto executionTimeAtomic = std::chrono::duration_cast<std::chrono::duration<double>>( timePointEnd - timePointBegin );


    std::cout << "Mutex took  " << executionTimeMutex.count() << " seconds." << std::endl;
    std::cout << "Atomic took " << executionTimeAtomic.count() << " seconds." << std::endl;
}

TEST( criticalSection, std_recursive_mutex__letsPlay )
{
    // What will be printed?

    std::recursive_mutex recursive_mutex;
    for ( uint64_t i = 0; i < 2; ++i)
    {
        recursive_mutex.lock();
        recursive_mutex.lock();
        std::cout<< "Main thread: after lock\n";

        auto future = std::async( std::launch::async, [&recursive_mutex]()
        {
            recursive_mutex.lock();
            recursive_mutex.lock();
            std::cout<< "Second thread: after lock\n";
            recursive_mutex.unlock();
            recursive_mutex.unlock();
            std::cout<< "Second thread: ending\n";
        } );

        std::cout<< "Main thread: after async\n";

        recursive_mutex.unlock();
        std::cout<< "Main thread: just before last unlock\n";
        recursive_mutex.unlock();
    }
}

TEST( criticalSection, std_atomic__trivialDataHolder )
{
    /*
     * Goal: Show that developer can create his own std::atomic variable with trivial data structure
    */

    struct TrivialDataHolder
    {
        uint32_t	m_iValue;
        float		m_fValue;
    };

    TrivialDataHolder dataHolder;

    std::atomic<TrivialDataHolder> atomicTrivialDataHolder;
    atomicTrivialDataHolder.store( dataHolder );
}

TEST( criticalSection, std_shared_mutex__rwlock )
{
    /*
     * Goal: Show more than one thread in read block and always one thread in write block.
    */

    std::shared_mutex rwMutex;
    auto pData = std::make_shared<uint32_t>( 0 );

    std::vector<std::future<void>> futures;
    std::atomic_uint32_t numberOfThreadsInsideReadBlock  = 0;
    std::atomic_uint32_t numberOfThreadsOutsideReadBlock = 0;

    std::atomic_uint32_t numberOfThreadsInsideWriteBlock  = 0;
    std::atomic_uint32_t numberOfThreadsOutsideWriteBlock = 0;

    const uint32_t numberOfThreads  = 10;

    auto Reader = [pData, &rwMutex, &numberOfThreadsOutsideReadBlock, &numberOfThreadsInsideReadBlock]( )
    {
        while (numberOfThreads < ++numberOfThreadsOutsideReadBlock);	// Heavy load wait

        rwMutex.lock_shared();
        ++numberOfThreadsInsideReadBlock;
        std::cout << "Inside read lock( " + std::to_string( numberOfThreadsInsideReadBlock ) + " ). Data: " + std::to_string( *pData ) + "\n";
        --numberOfThreadsInsideReadBlock;
        rwMutex.unlock_shared();
    };

    auto Writer = [pData, &rwMutex, &numberOfThreadsOutsideWriteBlock, &numberOfThreadsInsideWriteBlock]( )
    {
        while (numberOfThreads < ++numberOfThreadsOutsideWriteBlock);	// Heavy load wait

        rwMutex.lock();
        ++numberOfThreadsInsideWriteBlock;
        *pData += 1;
        std::cout << "Inside write lock( " + std::to_string( numberOfThreadsInsideWriteBlock ) + " ). Data: " + std::to_string( *pData ) + "\n";
        --numberOfThreadsInsideWriteBlock;
        rwMutex.unlock();
    };

    for ( uint32_t i = 0; i < numberOfThreads; ++i)
    {
        futures.emplace_back( std::async( std::launch::async, Reader ) );
        futures.emplace_back( std::async( std::launch::async, Writer ) );
    }

    for ( auto& future : futures ) future.wait();

    std::cout << "going out\n";
}

TEST( criticalSection, errorCollapsedScope_objectDestruction )
{
    /*
     * Goal: Show a little bit "not easy to find in battleground" problem with threads and rolled scope or collapsed stack.
    */

    struct ThreadWrap
    {
        ThreadWrap()
        {
            auto pData = std::make_shared<uint32_t>( 123 );
            m_aThreads.emplace_back( std::thread( [ &pData ]()
            {
                std::cout << "Inside thread0: " + std::to_string( *pData ) + "\n";		// What does the line print? 123? 666? memory violation?
            } ) );

            std::this_thread::sleep_for( std::chrono::nanoseconds( 1 ) );
            m_aFutures.emplace_back(  std::async( std::launch::async, [ &pData ]()
            {
                std::this_thread::sleep_for( std::chrono::milliseconds( 10 ) );			// Simulate long operation
                std::cout << "Inside thread1: " + std::to_string( *pData ) + "\n";		// What does the line print? 123? 666? memory violation?
            } ) );
        }

        ~ThreadWrap()
        {
            for ( auto& thread : m_aThreads ) thread.join();
        }
        std::vector<std::thread> m_aThreads;		// Just to show example of “broadening” life of a thread
        std::vector<std::future<void>> m_aFutures;	// from method scope to object ThreadWrap scope.
    };

    ThreadWrap threadWrap;

    auto pData1 = std::make_shared<uint32_t>( 666 );
    std::cout << "main thread " +  std::to_string( *pData1 ) + "\n";
}

TEST( criticalSection, errorCollapsedScope_inheritance )
{
    /*
     * Goal: Show a little bit "not easy to find in battleground" problem with threads and rolled scope or collapsed stack.
    */

    struct ThreadBase
    {
        virtual ~ThreadBase()
        {
            m_stopThread = true;
            if ( m_thread.joinable() ) m_thread.join();
        }

        std::atomic_bool	m_stopThread = false;
        std::thread			m_thread;
    };

    struct ThreadImplementation : public ThreadBase
    {
        ThreadImplementation()
        {
            m_futureStartThread = m_promiseStartThread.get_future();		// To sync start of the m_thread

            m_iImportantValue = 123;
            m_pImportantValue = std::make_unique<uint32_t>( 321 );
            m_thread = std::thread( [&]()
            {
                m_promiseStartThread.set_value();							// To sync start of the m_thread

                while( !m_stopThread )
                {
                    std::cout << "thread - stack: " + std::to_string( m_iImportantValue ) + "\n";
                    std::cout << "thread - heap:  " + std::to_string( *m_pImportantValue ) + "\n";
                }
            } );
        }
        volatile std::atomic_int	m_iImportantValue;
        std::promise<void>			m_promiseStartThread;
        std::future<void>			m_futureStartThread;
        std::unique_ptr<uint32_t>	m_pImportantValue;
    };

    ThreadImplementation threadImplementation;
    threadImplementation.m_futureStartThread.wait();

    auto pData1 = std::make_shared<uint32_t>( 666 );
    std::cout << "main thread " +  std::to_string( *pData1 ) + "\n";
}

TEST( criticalSection, errorCollapsedScope_orderOfPropertiesDestruction )
{
    /*
     * Goal: Show a little bit "not easy to find in the battleground" problem with threads and rolled scope or collapsed stack.
    */

    ThreadPool<std::shared_ptr< std::packaged_task<void()>>> threadPool;
    auto Function = []()
    {
        std::cout << "Sth to process by threadPool: \n";
    };

    const uint32_t numberOfTasks = 10;
    for ( uint32_t i = 0; i < numberOfTasks; ++i)
    {
        threadPool.AddJob( std::make_shared< std::packaged_task<void()>>( Function ) );
    }
}

TEST( criticalSection, std_mutex__deadlockWrongOrder )
{
    /*
     * Goal: Show deadlock with wrong order of acquiring std::mutex.
    */

    struct Account
    {
        uint32_t	m_iAmount = 1000;
        std::mutex	m_mutexAmount;
    };
    Account accounts[2];

    for ( uint32_t i = 0; i < 1; ++i )
    {
        auto TransferFromAtoB = []( Account& a_accountA, Account& a_accountB )
        {
            a_accountA.m_mutexAmount.lock();
            std::cout << "First locked  " << std::this_thread::get_id() << std::endl;
            a_accountB.m_mutexAmount.lock();
            std::cout << "Second locked " << std::this_thread::get_id() << std::endl;

            a_accountA.m_iAmount -= 666;
            a_accountB.m_iAmount += 666;

            a_accountB.m_mutexAmount.unlock();
            a_accountA.m_mutexAmount.unlock();

        };

        // Note: std::async doesn't take reference by default. We need to help with std::ref
        auto future0 = std::async( std::launch::async, TransferFromAtoB,
                                   std::ref( accounts[0] ), std::ref( accounts[1] ) );

        auto future1 = std::async( std::launch::async, TransferFromAtoB,
                                   std::ref( accounts[1] ), std::ref( accounts[0] ) );

        future0.wait();
        future1.wait();
        std::cout << accounts[0].m_iAmount << std::endl;
        std::cout << accounts[1].m_iAmount << std::endl;
    }
}

TEST( criticalSection, std_scoped_lock )
{
    /*
     * Goal: Show a way to acquire many std::mutex objs without deadlock.
    */

    struct Account
    {
        uint32_t	m_iAmount = 1000;
        std::mutex	m_mutexAmount;
    };
    Account accounts[2];

    for ( uint32_t i = 0; i < 100; ++i )
    {
        auto TransferFromAtoB = []( Account& a_accountA, Account& a_accountB )
        {
            std::scoped_lock scope( a_accountA.m_mutexAmount, a_accountB.m_mutexAmount );	// Acquire many std::mutex objs in an "atomic" mannier
            std::cout << "All locked " << std::this_thread::get_id() << std::endl;

            a_accountA.m_iAmount -= 123;
            a_accountB.m_iAmount += 123;
        };

        auto future0 = std::async( std::launch::async, TransferFromAtoB,
                                   std::ref( accounts[0] ), std::ref( accounts[1] ) );	// Note: std::async doesn't take reference by default. We need to help with std::ref

        auto future1 = std::async( std::launch::async, TransferFromAtoB,
                                   std::ref( accounts[1] ), std::ref( accounts[0] ) );	// Note: std::async doesn't take reference by default. We need to help with std::ref
        future0.wait();
        future1.wait();
        std::cout << accounts[0].m_iAmount << std::endl;
        std::cout << accounts[1].m_iAmount << std::endl;
    }
}
