#include "graph/graph.h"

#include "gtest/gtest.h"

namespace graph
{

TEST( Graph, print )
{
    Graph graph;
    graph.add("Node 1").add("Node 2").add("Node 3").add("Node 4").add("Node 5");
    graph.addConnection( "Node 2", { "Node 3", 22 } ).addConnection( "Node 2", { "Node 4", 24 } )
         .addConnection( "Node 2", { "Node 5", 25 } );
    graph.addConnection( "Node 5", { "Node 1", 51 } ).addConnection( "Node 5", { "Node 2", 52 } )
         .addConnection( "Node 3", { "Node 1", 31 } );
    std::cout << graph.toString() << std::endl;
}

TEST( Graph, depthFirstSearch)
{
    std::vector<std::vector<uint8_t>> gameBoard =
    {
        { 1, 7, 2, 9 },
        { 9, 7, 1, 3 },
        { 0, 0, 0, 5 },
        { 1, 1, 7, 1 },
        { 0, 1, 1, 3 }
    };
    auto pGraph = Graph::CreateGraph( gameBoard );
    DataSearch dataSearch;
    dataSearch.mIdNodeToStart = "y0x0";
    dataSearch.mIdNodeToFind  = "y3x0";
    pGraph->searchDFS( dataSearch );

    for ( const auto& solution : dataSearch.mSolutions )
    {
        std::cout << solution.toString() << std::endl;
    }

    std::cout << "Best one: " << dataSearch.mSolutionBestOne.toString() << std::endl;
    std::cout << pGraph->toString() << std::endl;
}

TEST( Graph, breadthFirstSearch)
{
    std::vector<std::vector<uint8_t>> gameBoard =
    {
        { 1, 7, 2, 9 },
        { 9, 7, 1, 3 },
        { 0, 0, 0, 5 },
        { 1, 1, 7, 1 },
        { 0, 1, 1, 3 }
    };
    auto pGraph = Graph::CreateGraph( gameBoard );
    DataSearch dataSearch;
    dataSearch.mIdNodeToStart = "y0x0";
    dataSearch.mIdNodeToFind  = "y3x0";
    pGraph->searchBFS( dataSearch );

    for ( const auto& solution : dataSearch.mSolutions )
    {
        std::cout << solution.toString() << std::endl;
    }

    std::cout << "Best one: " << dataSearch.mSolutionBestOne.toString() << std::endl;
    std::cout << pGraph->toString() << std::endl;
}

TEST( Graph, breadthFirstSearchOtherData )
{
    Graph graph;
    graph.add("A").add("B").add("C").add("D").add("E").add("F");
    graph.addConnection( "A", { "B", 5 } ).addConnection( "A", { "C", 20 } )
         .addConnection( "B", { "E", 4 } );
    graph.addConnection( "C", { "F", 3 } ).addConnection( "C", { "D", 15 } )
         .addConnection( "D", { "A", 1 } ).addConnection( "D", { "B", 7 } )
         .addConnection( "F", { "A", 40 } );

    DataSearch dataSearch;
    dataSearch.mIdNodeToStart = "A";
    dataSearch.mIdNodeToFind  = "A";
    graph.searchBFS( dataSearch );

    std::cout << "Best one: " << dataSearch.mSolutionBestOne.toString() << std::endl;
    std::cout << graph.toString() << std::endl;
}

}
