#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <cstring>
#include "gtest/gtest.h"

TEST( FileStream, textModeRead )
{
    std::ifstream stream("file.txt", std::ios::in);
    if (stream.good())
    {
        std::cout << "\nPrinting content of file in normal order (word by word):\n";
        std::string s;
        while (!stream.eof())
        {
            stream >> s;
            std::cout << s << " ";
        }

        std::cout << "\n\nPrinting content of file in reverse order (word by word):\n";
        stream.clear(); // clear eof bit
        stream.seekg(-1, std::ios::end);
        // linux file system specific:
        // stream.seekg( 0, std::ios::end) - eof,                 peek() -> -1
        // stream.seekg(-1, std::ios::end) - eol,                 peek() -> (dec)10, '\n'
        // stream.seekg(-2, std::ios::end) - last 'visible' char, peek() -> '.'
        s = "nothing was read";

        bool goThroughWhiteSpace = false;
        char c;
        while (!stream.fail())
        {
            c = stream.peek();
            std::cout << static_cast<int>(c) << " " << c << " ";
            if ( (c == ' ' || c == '\n') && goThroughWhiteSpace )
            {
                goThroughWhiteSpace = false;
            }
            else if (c == ' ' || c == '\n')
            {
                stream >> s;
                std::cout << s << " ";
                goThroughWhiteSpace = true;
            }
            else if (stream.tellg() == 0)
            {
                // Remember that reading will move cursor so seekg will not generate error in stream
                // and because of that break is needed.
                stream >> s;
                std::cout << s << " ";
                std::cout << stream.tellg();
                break;
            }

            stream.seekg(-1, std::ios::cur);
        }
    }

    stream.close();
}

TEST( FileStream, textModeReadStaticBuffor )
{
    std::ifstream stream("file.txt", std::ios::in);

    char buffor[32];
    while (stream.good())
    {
        stream.read(buffor, sizeof(buffor) - 1);
        buffor[stream.gcount()] = '\0';
        std::cout << buffor;
    }

    stream.close();
}

TEST( StringStream, charArrayToString )
{
    char tabsc[20] = "Test";
    tabsc[19] = 'k';
    std::stringstream sc(tabsc, std::ios_base::in | std::ios_base::out);
    std::cout << sc.str() << std::endl;

    sc.seekp(0, std::ios::end);
    sc << " Extra at the end.";
    std::cout << sc.str() << " | size: " << sc.str().size() << std::endl;

    std::strncpy(tabsc, sc.str().data(), 19);
    tabsc[19] = '\0'; // 20th character is under 19 index and '\0' is needed for proper string representation.
    std::cout << tabsc << " " << std::endl;
}

TEST( StringStream, stringToDouble )
{
    std::istringstream str("0.1 0.2 0.3 0.4");
    std::partial_sum(std::istream_iterator<double>(str),
                        std::istream_iterator<double>(),
                        std::ostream_iterator<double>(std::cout, " ")); //ostream_iterator is quicker
}