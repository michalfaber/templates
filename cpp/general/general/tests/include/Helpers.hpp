#pragma once

#include <iostream>
#include <fstream>
#include <thread>
#include <future>
#include <shared_mutex>
#include <queue>


// Maybe not needed.
void FunctionWriteToFile()
{
	std::this_thread::sleep_for( std::chrono::seconds( 2 ) );		// Just to simulate long operation. This is no good way of synchronizing

	std::ofstream myfile;
	myfile.open ("debugOutput.txt");
	myfile << "Where is our detached thread? TID: " << std::this_thread::get_id() << std::endl;
	myfile.close();
}

void Function( int32_t a_iArg )
{
	std::cout << __PRETTY_FUNCTION__ << ": " << a_iArg << std::endl;
}

void FunctionWholeString( int32_t a_iArg )
{
	std::cout << std::string( __PRETTY_FUNCTION__ ) + " " + std::to_string( a_iArg ) + "\n";
}

struct Callable
{
	void Method( uint32_t a_fArg )
	{
		std::cout << __PRETTY_FUNCTION__ << ": " << a_fArg << std::endl;
	}

	void MethodWholeString( uint32_t a_fArg )
	{
		std::cout << std::string( __PRETTY_FUNCTION__ ) + " " + std::to_string( a_fArg ) + "\n";
	}
};


template< typename Type>
class ThreadSafeProperty
{
public:
	void SetValue( Type a_iValue )
	{
		std::unique_lock writeLockValue( m_sharedMutexValue );	// Notice that here is unique_lock which works like write lock...
		m_iValue += a_iValue;
	}

	Type GetValue() const
	{
		std::shared_lock readLockValue( m_sharedMutexValue );	// ... and here is shared_lock which works like read lock
		return m_iValue;
	}

	std::string ToString() const
	{
		std::shared_lock readLockValue( m_sharedMutexValue );
		return std::string( __PRETTY_FUNCTION__ ) + "::m_iValue = " + std::to_string( m_iValue );
	}

private:
	Type						m_iValue = 0;		// Or std::atomic_int32_t and in such case there is no need for std::mutex
	mutable std::shared_mutex	m_sharedMutexValue;	// Also std::mutex with std::lock_guard may be used insted.
};



// This one is not so easy to go through and is not obvious one
template <typename JobType>
class JobsQueue
{
public:
	JobsQueue() = default;
	~JobsQueue() = default;

	template< typename JobTypeForwaring>
	void AddJob( JobTypeForwaring&& a_job )
	{
		std::lock_guard guard( m_mutexJobs );
		m_aJobs.emplace( std::forward<JobTypeForwaring>( a_job ) );
	}

	JobType GetOne()
	{
		std::lock_guard guard( m_mutexJobs );
		if ( m_aJobs.empty() ) return nullptr;

		auto job = m_aJobs.front();
		m_aJobs.pop();

		return job;
	}

	bool Empty()
	{
		std::lock_guard guard( m_mutexJobs );
		return m_aJobs.empty();
	}

private:
	std::mutex m_mutexJobs;
	std::queue<JobType> m_aJobs;
};

template <typename JobType>
class ThreadPool
{
public:
	ThreadPool()
	{
		constexpr const uint32_t numberOfThreads = 4;
		for ( uint32_t i = 0; i < numberOfThreads; ++i )
		{
			m_aFuturesAsync.emplace_back( std::async( std::launch::async, [this]
			{
				while ( true )
				{
					std::unique_lock lockSync( m_mutexSync );
					while ( m_aJobs.Empty() )
					{
						if ( m_bFinish )	// End of the thread happens when m_aJobs is empty and m_bFinish is set true.
						{
							return;
						}
						m_cvSync.wait( lockSync );
					}
					lockSync.unlock();

					auto pJobToExecute = m_aJobs.GetOne();
					if ( pJobToExecute )
					{
						(*pJobToExecute)();
					}
				}
			} ) );
		}
	}

	~ThreadPool()
	{
		std::unique_lock lockSync( m_mutexSync );
		m_bFinish = true;
		// unlock must happen before notify_all which wakeups all threads and they should end because of m_bFinish.
		lockSync.unlock();

		m_cvSync.notify_all();
	}

	template<typename JobTypeForwaring>
	void AddJob( JobTypeForwaring&& a_job )
	{
		m_aJobs.AddJob( std::forward<JobTypeForwaring>( a_job ) );
		m_cvSync.notify_one();
	}

private:
	bool							m_bFinish = false;
	std::mutex						m_mutexSync;
	std::condition_variable			m_cvSync;
	std::vector<std::future<void>>	m_aFuturesAsync;
	JobsQueue<JobType>				m_aJobs;
};
