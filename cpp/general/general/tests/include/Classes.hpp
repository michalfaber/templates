#pragma once

#include <iostream>
#include <memory>
#include <vector>
#include "../../common/Figure.hpp"

/// IsDerivative
class Animal
{
public:
    Animal() = default;
    virtual ~Animal() { std::cout << __PRETTY_FUNCTION__ << std::endl; }

    virtual void speak() { std::cout << __PRETTY_FUNCTION__ << std::endl;}
};

class Dog : public Animal
{
public:
    Dog() = default;
    ~Dog() override { std::cout << __PRETTY_FUNCTION__ << std::endl; }

    void speak() override { std::cout << __PRETTY_FUNCTION__ << std::endl; }
};

class Cow   // Note there is no inheritance!
{
public:
    Cow() = default;
    ~Cow() { std::cout << __PRETTY_FUNCTION__ << std::endl; }
};

template<class _Derivative_, class _BaseClass_>
bool IsDerivative()
{
    bool isDerived = false;

    try
    {
        if ( std::dynamic_pointer_cast<_BaseClass_>( std::make_shared<_Derivative_>() ) ){ isDerived = true; }
    }
    catch ( std::bad_alloc& exception )
    { 
        std::cout << __FUNCTION__ << " catched: " << exception.what() << std::endl; 
    }
    catch ( ... )
    { 
        std::cout << __FUNCTION__ << " catched: unknown error" << std::endl; 
    }

    return isDerived;
};


/// Definition of method in PureVirtual class
struct PureVirtual
{
    virtual void methodDoSth() = 0;
};

struct PureVirtualDerivative : public PureVirtual
{
    void methodDoSth() override
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
    }
};


/// Example of a template with method specialization
// Remember that template methods declaration and definition
// must be written down in one, the same file.
template<class Type>
struct Storage
{
    explicit Storage( Type aValue ) { this->mValue = aValue; }
    Type getValue() const           { return this->mValue;   }

    Type mValue;
};


/// Move constructor and operator
struct Amove 
{
    Amove( std::string s = "") noexcept : _s(s) {}
    Amove( const Amove& o    ) noexcept : _s(__PRETTY_FUNCTION__) {}
    Amove( Amove&& o         ) noexcept : _s(__PRETTY_FUNCTION__) {}
    ~Amove() = default;

    const std::string& get_s() const noexcept { return _s; }

    Amove& operator=( const Amove& o ) noexcept 
    {
        this->_s = __PRETTY_FUNCTION__;
        return *this;
    }

    Amove& operator=( Amove&& o ) noexcept
    {
        this->_s = __PRETTY_FUNCTION__;
        return *this;
    }

    std::string _s;
};


/// noexcept

class ClassWithNoexcept
{
public:
    ClassWithNoexcept() noexcept                             { std::cout << __PRETTY_FUNCTION__ << std::endl; }
    ClassWithNoexcept(const ClassWithNoexcept& obj) noexcept { std::cout << __PRETTY_FUNCTION__ << std::endl; }
    ClassWithNoexcept(ClassWithNoexcept&& obj) noexcept      { std::cout << __PRETTY_FUNCTION__ << std::endl; }
};

class ClassWithoutNoexcept
{
public:
    ClassWithoutNoexcept()                                  { std::cout << __PRETTY_FUNCTION__ << std::endl; }
    ClassWithoutNoexcept( const ClassWithoutNoexcept& obj ) { std::cout << __PRETTY_FUNCTION__ << std::endl; }
    ClassWithoutNoexcept( ClassWithoutNoexcept&& obj )      { std::cout << __PRETTY_FUNCTION__ << std::endl; }
};


/// copy operator
class BaseClassOpCopy{
public:
    // _copy() not required for base class
    virtual const BaseClassOpCopy& operator= (const BaseClassOpCopy& b)
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
        if (this != &b)
        {
            mValueFromBase = b.mValueFromBase;
        }
        return *this;
    }

    uint32_t mValueFromBase;
};


// Copy method usage
class DerivativeClass1 : public BaseClassOpCopy{
private:
    void _copy(const DerivativeClass1& d1)
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
        mValueFromDerivative1 = d1.mValueFromDerivative1;
    }

public:
    const DerivativeClass1& operator= (const BaseClassOpCopy& b) override
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
        if (this != &b)
        {
            BaseClassOpCopy::operator=(b);
            try
            {
                _copy(dynamic_cast<const DerivativeClass1 &>(b));
            }
            catch (std::bad_cast &)
            {
                // Set defaults or do nothing.
                mValueFromDerivative1 = 222;
            }
        }
        return *this;
    }

    virtual const DerivativeClass1& operator= (const DerivativeClass1& d1)
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
        if (this != &d1)
        {
            BaseClassOpCopy::operator=(d1);
            _copy(d1);
        }
        return *this;
    }

    int mValueFromDerivative1;
};

class DerivativeClass2 : public DerivativeClass1{
private:
    void _copy(const DerivativeClass2& d2)
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
        mValueFromDerivative2 = d2.mValueFromDerivative2;
    }

public:
    // Top-most superclass operator= definition
    const DerivativeClass2& operator= (const BaseClassOpCopy& b) override
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
        if (this != &b)
        {
            DerivativeClass1::operator=(b);
            try
            {
                _copy(dynamic_cast<const DerivativeClass2 &>(b));
            }
            catch (std::bad_cast&)
            {
                // Set defaults or do nothing
                mValueFromDerivative2 = 333;
            }
        }
        return *this;
    }

    // Same body for other superclass arguments
    const DerivativeClass2& operator= (const DerivativeClass1& d1) override
    {
        // Conversion to superclass reference
        // should not throw exception.
        // Call base operator() overload.
        std::cout << __PRETTY_FUNCTION__ << std::endl;
        if (this == &d1) return *this;
        return DerivativeClass2::operator= (dynamic_cast<const BaseClassOpCopy&>(d1));
    }

    // The current class operator=
    virtual const DerivativeClass2& operator= (const DerivativeClass2& d2)
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
        if (this != &d2)
        {
            DerivativeClass1::operator =(d2);
            _copy(d2);
        }
        return *this;
    }

    int mValueFromDerivative2;
};


/// Defining units
class Degree
{
public:
    Degree(long double deg) : mDegree(deg){}
    Degree& operator++(){ this->mDegree++; return *this;}

    long double get() const { return this->mDegree; }

private:
    long double mDegree;
};