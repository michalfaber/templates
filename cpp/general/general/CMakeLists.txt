cmake_minimum_required(VERSION 3.5.1)

project( test_general_all )
add_executable( ${PROJECT_NAME} "" )
target_include_directories( ${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_LIST_DIR}/tests )

find_library( LIB_PTHREAD    pthread    )
find_library( LIB_GTEST      gtest      )
find_library( LIB_GTEST_MAIN gtest_main )
find_library( LIB_GMOCK      gmock      )

target_link_libraries( ${PROJECT_NAME}
    PRIVATE
        ${LIB_PTHREAD}
        ${LIB_GTEST}
        ${LIB_GTEST_MAIN}
        ${LIB_GMOCK}
        pthread
        gcov                # dont know why.
)

target_sources( ${PROJECT_NAME}
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/main.cpp

        ${CMAKE_CURRENT_LIST_DIR}/graph/connection_full.h
        ${CMAKE_CURRENT_LIST_DIR}/graph/connection_full.cpp
        ${CMAKE_CURRENT_LIST_DIR}/graph/connection_half.h
        ${CMAKE_CURRENT_LIST_DIR}/graph/connection_half.cpp
        ${CMAKE_CURRENT_LIST_DIR}/graph/graph.h
        ${CMAKE_CURRENT_LIST_DIR}/graph/graph.cpp
        ${CMAKE_CURRENT_LIST_DIR}/graph/data_search.h
        ${CMAKE_CURRENT_LIST_DIR}/graph/data_search.cpp
        ${CMAKE_CURRENT_LIST_DIR}/graph/node.h
        ${CMAKE_CURRENT_LIST_DIR}/graph/node.cpp
        ${CMAKE_CURRENT_LIST_DIR}/graph/solution.h
        ${CMAKE_CURRENT_LIST_DIR}/graph/solution.cpp
        ${CMAKE_CURRENT_LIST_DIR}/graph/hasher_node.h
        ${CMAKE_CURRENT_LIST_DIR}/graph/comparator_node.h

        ${CMAKE_CURRENT_LIST_DIR}/app_multithreading/message/pipe.h
        ${CMAKE_CURRENT_LIST_DIR}/app_multithreading/message/propagator.h
        ${CMAKE_CURRENT_LIST_DIR}/app_multithreading/component/process_data.h
        ${CMAKE_CURRENT_LIST_DIR}/app_multithreading/component/gate.h
        ${CMAKE_CURRENT_LIST_DIR}/app_multithreading/component/loop_enter.h
        ${CMAKE_CURRENT_LIST_DIR}/app_multithreading/component/loop_exit.h
        ${CMAKE_CURRENT_LIST_DIR}/app_multithreading/component/screen.h
        ${CMAKE_CURRENT_LIST_DIR}/app_multithreading/parallel/executor.h

        ${CMAKE_CURRENT_LIST_DIR}/tests/include/Classes.hpp
        ${CMAKE_CURRENT_LIST_DIR}/tests/include/Helpers.hpp
        
        ${CMAKE_CURRENT_LIST_DIR}/tests/src/Classes.cpp
        ${CMAKE_CURRENT_LIST_DIR}/tests/src/Concurrency.cpp
        ${CMAKE_CURRENT_LIST_DIR}/tests/src/Containers.cpp
        ${CMAKE_CURRENT_LIST_DIR}/tests/src/DataStructures.cpp
        ${CMAKE_CURRENT_LIST_DIR}/tests/src/DesingPatterns.cpp
        ${CMAKE_CURRENT_LIST_DIR}/tests/src/Exceptions.cpp
        ${CMAKE_CURRENT_LIST_DIR}/tests/src/GameBoard.cpp
        ${CMAKE_CURRENT_LIST_DIR}/tests/src/Graph.cpp
        ${CMAKE_CURRENT_LIST_DIR}/tests/src/RandomStuff.cpp
        ${CMAKE_CURRENT_LIST_DIR}/tests/src/SmartPtr.cpp
        ${CMAKE_CURRENT_LIST_DIR}/tests/src/Streams.cpp
        ${CMAKE_CURRENT_LIST_DIR}/tests/src/parallel_execution.cpp
)


install( TARGETS ${PROJECT_NAME} DESTINATION bin )
