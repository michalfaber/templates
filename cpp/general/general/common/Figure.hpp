#pragma once
#include <iostream>

/// - Example of smart ptr with class
/// - Unordered_set and unordered_map so definition of
///   method for comparing and method for hashing
class Figure
{
public:
    virtual ~Figure() = default;

    virtual float getSurface()   const = 0;
    virtual float getPerimeter() const = 0;
};

class Square : public Figure
{
public:
    explicit Square( float side ){ this->mSide = side; }
    ~Square() override { std::cout << __PRETTY_FUNCTION__ << std::endl; }

    float getSurface()   const override { return mSide * mSide; }
    float getPerimeter() const override { return 4.0f  * mSide; }
    float getSide()      const          { return mSide;}

    bool operator==( const Square &a ) const
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
        return (this->mSide * this->mSide) == (a.mSide * a.mSide);
    }

    bool operator<( const Square &a ) const
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
        return (this->mSide * this->mSide) < (a.mSide * a.mSide);
    }

private:
    float mSide;
};

namespace std       //For unordered_set<Square>
{
template<>
struct hash<Square>
{
    size_t operator()( Square const &x ) const noexcept
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
        return  ( 51 + std::hash<float>()( x.getSide() ) ) * 51
                + std::hash<float>()(x.getSide())
        ;
    }
};
}
