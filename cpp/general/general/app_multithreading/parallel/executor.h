#pragma once
#include <atomic>
#include <functional>
#include <memory>
#include <thread>

namespace parallel
{

struct Executor
{
public:
    Executor( const std::function<void( )>&    aWork,
              const std::chrono::milliseconds& aDelayThread = std::chrono::milliseconds( 10 ) )
        : mWork {aWork}
        , mDelayThread {aDelayThread}
        , mExecute {false}
    {
        if ( ! mWork )
            throw "nullptr work in Executor";
    }

    ~Executor( ) { endExecution( ); }

    void startExecution( )
    {
        if ( mExecute )
        {
            throw "Already executing";
        }

        mThread = std::thread( [this]( ) {
            while ( mExecute )
            {
                mWork( );
                std::this_thread::sleep_for( mDelayThread );
            }
        } );

        mExecute = true;
    }

    void endExecution( )
    {
        mExecute = false;
        if ( mThread.joinable( ) )
        {
            mThread.join( );
        }
    }

private:
    std::function<void( )>    mWork;
    std::chrono::milliseconds mDelayThread;
    std::thread               mThread;
    std::atomic_bool          mExecute;
};

}
