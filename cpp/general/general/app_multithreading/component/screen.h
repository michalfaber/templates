#pragma once
#include "app_multithreading/message/pipe.h"
#include <iostream>

namespace component
{

class Screen
{
public:
    Screen( std::shared_ptr<message::Pipe<bool>> aMessageLoopEnter,
            std::shared_ptr<message::Pipe<bool>> aMessageLoopExit,
            std::shared_ptr<message::Pipe<bool>> aMessageGate )
        : mMessageLoopEnter {aMessageLoopEnter}
        , mMessageLoopExit {aMessageLoopExit}
        , mMessageGate {aMessageGate}
    {
    }

    void update( )
    {
        handleMessage( "Loop enter: ",
                       mMessageLoopEnter->getMessageBlocking( std::chrono::milliseconds {1} ) );
        handleMessage( "Loop exit: ",
                       mMessageLoopExit->getMessageBlocking( std::chrono::milliseconds {1} ) );
        handleMessage( "Gate: ",
                       mMessageGate->getMessageBlocking( std::chrono::milliseconds {1} ) );
    }

private:
    void handleMessage( const std::string& aTextTitle, std::unique_ptr<bool> mState )
    {
        if ( ! mState )
            return;

        std::cout << aTextTitle << *mState << std::endl;
    }

    std::shared_ptr<message::Pipe<bool>> mMessageLoopEnter;
    std::shared_ptr<message::Pipe<bool>> mMessageLoopExit;
    std::shared_ptr<message::Pipe<bool>> mMessageGate;
};

}
