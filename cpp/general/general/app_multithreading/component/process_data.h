#pragma once
#include <iostream>
#include <thread>

namespace component
{

class ProcessData //: public Worker
{
public:
    void work( ) // override
    {
        std::cout << "Start of processing " << mIteration << std::endl;
        std::this_thread::sleep_for( std::chrono::milliseconds {500} );
        std::cout << "End of processing " << mIteration++ << std::endl;
    }

private:
    uint32_t mIteration = 0;
};

}
