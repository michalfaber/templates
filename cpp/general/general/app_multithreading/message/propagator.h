#pragma once
#include "app_multithreading/message/pipe.h"
#include <iostream>
#include <memory>
#include <queue>
namespace message
{

template <typename TMessage>
class Propagator
{
public:
    void propagate( )
    {
        std::unique_lock lockGuard( mMutexMessages );
        for ( const auto& message : mMessages )
            for ( auto pipe : mPipes )
                pipe->addMessage( std::make_unique<TMessage>( *message ) );

        mMessages.clear( );
    }

    void propagateBlocking( std::chrono::milliseconds aTimeout = std::chrono::milliseconds {100} )
    {
        std::unique_lock lockGuard( mMutexMessages );
        mCvMessages.wait_for( lockGuard, aTimeout, [this]( ) { return ! mMessages.empty( ); } );

        for ( const auto& message : mMessages )
            for ( auto pipe : mPipes )
                pipe->addMessage( std::make_unique<TMessage>( *message ) );

        mMessages.clear( );
    }

    void addMessage( std::unique_ptr<TMessage> aMessage )
    {
        std::unique_lock lockGuard( mMutexMessages );
        mMessages.emplace_back( std::move( aMessage ) );
        lockGuard.unlock( );
        mCvMessages.notify_all( );
    }

    std::shared_ptr<Pipe<TMessage>> createPipe( )
    {
        auto messagePipe = std::make_shared<Pipe<TMessage>>( );

        mMutexMessages.lock( );
        mPipes.push_back( messagePipe );
        mMutexMessages.unlock( );

        return messagePipe;
    }

private:
    std::vector<std::unique_ptr<TMessage>>       mMessages;
    std::vector<std::shared_ptr<Pipe<TMessage>>> mPipes;
    std::mutex                                   mMutexMessages;
    std::condition_variable                      mCvMessages;
};

}
