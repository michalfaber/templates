#pragma once
#include <condition_variable>
#include <mutex>
#include <queue>

// todo: monitor design pattern

namespace message
{

template <typename TMessage>
class Pipe
{
public:
    void addMessage( std::unique_ptr<TMessage> aMessage )
    {
        std::lock_guard lockGuard( mMutexMessages );
        mMessages.emplace( std::move( aMessage ) );
        mCvMessages.notify_all( );
    }

    std::unique_ptr<TMessage> getMessage( )
    {
        std::lock_guard           lockGuard( mMutexMessages );
        std::unique_ptr<TMessage> returnMessage = nullptr;
        if ( ! mMessages.empty( ) )
        {
            returnMessage = std::move( mMessages.front( ) );
            mMessages.pop( );
        }
        return std::move( returnMessage );
    }

    std::unique_ptr<TMessage> getMessageBlocking( std::chrono::milliseconds aTimeout )
    {
        std::unique_lock lockGuard( mMutexMessages );
        mCvMessages.wait_for( lockGuard, aTimeout, [this]( ) { return ! mMessages.empty( ); } );

        std::unique_ptr<TMessage> returnMessage = nullptr;
        if ( ! mMessages.empty( ) )
        {
            returnMessage = std::move( mMessages.front( ) );
            mMessages.pop( );
        }
        return std::move( returnMessage );
    }

private:
    std::queue<std::unique_ptr<TMessage>> mMessages;
    std::mutex                            mMutexMessages;
    std::condition_variable               mCvMessages;
};

}
