#include <iostream>

#define UNW_LOCAL_ONLY
#include <libunwind.h>

void show_backtrace() 
{
    unw_cursor_t    cursor; 
    unw_context_t   uc;
    unw_word_t      ip, sp;

    unw_getcontext( &uc );
    unw_init_local( &cursor, &uc );
    while ( unw_step( &cursor ) > 0 ) 
    {
        unw_get_reg( &cursor, UNW_REG_IP, &ip );
        unw_get_reg( &cursor, UNW_REG_SP, &sp );
        printf ( "ip = %lx, sp = %lx\n", ( long ) ip, ( long ) sp );
        std::cout << "ip = "    << static_cast<long>( ip ) 
                  << " , sp = " << static_cast<long>( sp ) 
                  << std::endl;
    }
}

void justToTestBacktrace()
{
    std::cout << "eee makarena!" << std::endl;
    show_backtrace();
}

int main( int argc, char** argv )
{
    justToTestBacktrace();
    return 0;
}