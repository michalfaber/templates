import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.13

ApplicationWindow
{
    visible: true
    width:   640
    height:  480
    title:   qsTr( "Scroll" )

    ScrollView
    {
        anchors.fill: parent

        ListView
        {
            width:     parent.width
            model:     20
            delegate:  Button
            {
                id: idButton
                text:  "Item " + (index + 1)
                width: parent.width

                onClicked: idPopup.open()
            }
        }
    }

    Popup
    {
        id:               idPopup
        anchors.centerIn: parent

        width:  300
        height: 200
        modal:  true

        contentItem: ColumnLayout
        {
            anchors.fill: parent

            Label
            {
                Layout.fillWidth:  true
                Layout.fillHeight: true
                text:              "Info popup"
                Layout.margins:    10
            }

            Button
            {
                Layout.alignment: Qt.AlignHCenter
                Layout.margins:   10
                width:            100
                height:           50
                text:             "Close"

                onClicked: idPopup.close()
            }
        }
    }
}
