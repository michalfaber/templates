#pragma once
#include "gmock/gmock.h"
#include "tools/Point.hpp"

using ::testing::_;

class MockPoint : public Point
{
public:
    MockPoint( double x, double y ) : Point( x, y ){}
    MOCK_METHOD0( toString, std::string() );
};
