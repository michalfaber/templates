cmake_minimum_required( VERSION 3.6 )

set( CHECK_ALL check_all )
set( MEMCHECK_ALL memcheck_all )

add_custom_target( ${CHECK_ALL} )
add_custom_target( ${MEMCHECK_ALL} )

add_subdirectory( figures )
add_subdirectory( tools )