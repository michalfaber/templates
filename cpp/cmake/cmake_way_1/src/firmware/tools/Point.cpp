#include "tools/Point.hpp"

Point::Point( double x, double y ) : mX(x), mY(y){}

std::string Point::toString() const
{
    return "Point: [" + std::to_string(mX) + "; " + std::to_string(mY) + "]";
}

