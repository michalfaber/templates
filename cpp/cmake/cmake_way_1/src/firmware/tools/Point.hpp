#pragma once
#include <string>

class Point
{
public:
    Point( double x = 0.0, double y = 0.0 );
    virtual ~Point() = default;
    virtual std::string toString() const;

private:
    double mX, mY;
};
