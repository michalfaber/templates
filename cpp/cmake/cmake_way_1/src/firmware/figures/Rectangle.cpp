#include "figures/Rectangle.hpp"

Rectangle::Rectangle( std::array<Point, 4> aPoints )
{
    mPoints = aPoints;
}

std::string Rectangle::toString() const
{
    std::string strRepresentation = "Rectangle:";
    for ( const auto& point : mPoints )
    {
        strRepresentation += "\n\t" + point.toString();
    }
    return strRepresentation;
}

