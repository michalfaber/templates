#pragma once
#include "tools/Point.hpp"
#include <string>
#include <array>

class Rectangle
{
public:
    Rectangle( std::array<Point, 4> aPoints );
    virtual ~Rectangle() = default;
    virtual std::string toString() const;

private:
    std::array<Point, 4> mPoints;
};
