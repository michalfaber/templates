cmake_minimum_required(VERSION 3.6)

project( figures )
add_library( ${PROJECT_NAME} SHARED "")
target_include_directories( ${PROJECT_NAME} PUBLIC ${CMAKE_CURRENT_LIST_DIR}/.. )

target_link_libraries( ${PROJECT_NAME}
    PUBLIC
        tools
)

set( PUBLIC_HEADER_FILES
        ${CMAKE_CURRENT_LIST_DIR}/Rectangle.hpp
)

target_sources( ${PROJECT_NAME}
    PUBLIC
        ${PUBLIC_HEADER_FILES}
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/Rectangle.cpp
)

set_target_properties( ${PROJECT_NAME} PROPERTIES VERSION 0.0.1 SOVERSION 0.0.1 )

install( TARGETS ${PROJECT_NAME}        DESTINATION lib )
install( FILES   ${PUBLIC_HEADER_FILES} DESTINATION include/figures )
