#include "figures/Rectangle.hpp"
#include <iostream>

int main( int vargs, const char** args )
{
    std::cout << vargs   << std::endl;
    std::cout << args[0] << std::endl;

    std::array<Point, 4> points { Point{ 1, 1 }, { 2, 2 }, { 3, 3 }, { 4, 4 } };
    Rectangle rec( points );
    std::cout << rec.toString() << std::endl;

    return 0;
}


