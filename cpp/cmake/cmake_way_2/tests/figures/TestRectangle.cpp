#include "gtest/gtest.h"
#include "figures/Rectangle.hpp"

TEST( Figures, Rectangle )
{
    std::array<Point, 4> points { Point{ 1, 1 }, { 2, 2 }, { 3, 3 }, { 4, 4 } };
    Rectangle rec( points );
    std::cout << rec.toString() << std::endl;
}
