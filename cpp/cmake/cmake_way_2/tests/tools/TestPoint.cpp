#include "gtest/gtest.h"
#include "MockPoint.hpp"
#include "tools/Point.hpp"

using ::testing::_;
using ::testing::Return;

class ToolsTestPoint : public ::testing::Test
{
public:
    void SetUp() override
    {
        mPoint = std::make_unique<Point>(2.0, 4.0);
        std::cout << __PRETTY_FUNCTION__ << std::endl;
    }

    void TearDown() override
    {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
    }

protected:
    std::unique_ptr<Point> mPoint;
};

TEST_F( ToolsTestPoint, toString)
{
    std::cout << mPoint->toString() << std::endl;
    EXPECT_TRUE( mPoint->toString() == "Point: [2.000000; 4.000000]" );
}

TEST_F( ToolsTestPoint, Mock)
{
    MockPoint mockPoint{1.0, 2.0};

    EXPECT_CALL( mockPoint, toString()).Times( testing::AtLeast( 1 ) );
    ON_CALL( mockPoint, toString() ).WillByDefault( testing::Invoke(
    [] () { return std::string( "Mocked function toString" ); } ) );

    EXPECT_STREQ( "Mocked function toString", mockPoint.toString().c_str() );
}
