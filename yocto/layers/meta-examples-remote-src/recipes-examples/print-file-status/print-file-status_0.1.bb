SUMMARY = "bitbake-layers recipe"
DESCRIPTION = "Recipe uses locally defined soure code."
LICENSE = "CLOSED"

SRC_URI = "git://git@github.com/MichalFaberr/templates;branch=master;protocol=ssh"
PV ="1.0+git${SRCPV}"
SRCREV = "6824866d77e511380fc682de888142cce31b4437"
S = "${WORKDIR}/git/cpp/printFileStatus/printFileStatus"

inherit cmake
EXTRA_OECMAKE = " -DCMAKE_CXX_STANDARD=17 -DCMAKE_CXX_STANDARD_REQUIRED=ON"