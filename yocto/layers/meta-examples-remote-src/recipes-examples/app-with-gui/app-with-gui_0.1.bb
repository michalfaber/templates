SUMMARY	= "bitbake-layers recipe"
DESCRIPTION	= "Example app with qt and qml."
LICENSE = "CLOSED"

SRC_URI = "git://git@github.com/MichalFaberr/templates;branch=master;protocol=ssh"
PV ="1.0+git${SRCPV}"
SRCREV = "6824866d77e511380fc682de888142cce31b4437"
S = "${WORKDIR}/git/cpp/appWithGui/appWithGui"

inherit cmake clang cmake_qt5 

DEPENDS = "				            \
	qtbase				            \
	qtdeclarative		            \
    qtquickcontrols2	            \
"

RDEPENDS_${PN} = "		            \
	qtbase				            \
	qtdeclarative		            \
    qtquickcontrols2	            \
"

COMPILER  = "clang"
TOOLCHAIN = "clang"
EXTRA_OECMAKE += "-DCMAKE_CXX_STANDARD=17 -DCMAKE_CXX_STANDARD_REQUIRED=ON -DCMAKE_AUTOMOC:BOOL=ON -DCMAKE_AUTORCC:BOOL=ON"
