SUMMARY = "bitbake-layers recipe"
DESCRIPTION = "Recipe uses locally defined soure code."
LICENSE = "CLOSED"

SRC_URI = "git://git@github.com/MichalFaberr/templates;branch=master;protocol=ssh"
PV ="1.0+git${SRCPV}"
SRCREV = "6824866d77e511380fc682de888142cce31b4437"
S = "${WORKDIR}/git/cpp/printProgramName/printProgramName"

inherit cmake
#EXTRA_OECMAKE = ""	// specify any options you want to pass to cmake