SUMMARY = "bitbake-layers recipe"
DESCRIPTION = "Recipe uses locally defined soure code."
LICENSE = "CLOSED"

SRC_URI = "file:///${COREBASE}/meta-examples-local-src/src/printFigure/printFigure"
S = "${WORKDIR}/${COREBASE}/meta-examples-local-src/src/printFigure/printFigure"

DEPENDS = "library-figures"

inherit cmake
#EXTRA_OECMAKE = ""	// specify any options you want to pass to cmake