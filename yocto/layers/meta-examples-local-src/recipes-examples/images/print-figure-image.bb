require recipes-core/images/core-image-minimal.bb 
# print-figure-dev will install all needed headers on target image
IMAGE_INSTALL += "print-figure print-figure-dev bash"
IMAGE_FEATURES += "ssh-server-dropbear"

TOOLCHAIN_TARGET_TASK += " print-figure"
