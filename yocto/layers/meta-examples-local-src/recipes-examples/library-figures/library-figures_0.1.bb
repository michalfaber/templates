SUMMARY = "bitbake-layers recipe"
DESCRIPTION = "Recipe uses locally defined soure code."
LICENSE = "CLOSED"

SRC_URI = "file:///${COREBASE}/meta-examples-local-src/src/libraryFigures/libraryFigures"
S = "${WORKDIR}/${COREBASE}/meta-examples-local-src/src/libraryFigures/libraryFigures"

inherit cmake
#EXTRA_OECMAKE = ""	// specify any options you want to pass to cmake
