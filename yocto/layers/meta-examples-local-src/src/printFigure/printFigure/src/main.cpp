#include "figures/Rectangle.hpp"
#include <iostream>

int main( int argc, char** argv )
{
    std::array<Point, 4> points{ Point{ 1.2, 1.4 }
                               , Point{ 1.2, 1.4 }
                               , Point{ 1.2, 1.4 }
                               , Point{ 1.2, 1.4 } };
    Rectangle rectangle( points );
    std::cout << rectangle.toString() << std::endl;
    return 0;
}
