SCRIPT_DIR=$( dirname $( realpath $0) )                           # path to dir under which the currently executed script is located 
BUILD_DIR="$( realpath $SCRIPT_DIR/../cpp/cmake/build_x86_all )"  # path to dir where a deploy of the build was done 
SRC_DIR="$( realpath $SCRIPT_DIR/../cpp/cmake/cmake_way_1 )"      # path to dir with CMakeLists.txt

CXX_STANDARD=17
CORES=4

TARGET='all'
BUILD_DIR="$( realpath $SCRIPT_DIR/../cpp/cmake/build_x86_all )"  # path to dir where a deploy of build was done 
SRC_DIR="$( realpath $SCRIPT_DIR/../cpp/cmake/cmake_way_1 )"      # path to dir with CMakeLists.txt
BUILD_TYPE='Release'
INSTALL_DIR=`pwd`'/install/x86'

CMAKE='/usr/bin/cmake'

QT_DIR_X86='/home/michal/tools/qtlibs/5.14.0/gcc_64'
GTESTS_X86='/home/michal/projects/qt/googletest/install/x86'
QT_BUILD=OFF

CMAKE_FLAGS_COMMON=(
	$SRC_DIR
	-DCMAKE_INSTALL_PREFIX=$INSTALL_DIR/$PLATFORM
	-DCMAKE_CXX_STANDARD=$CXX_STANDARD
	-DCMAKE_CXX_STANDARD_REQUIRED=ON
	-DCMAKE_CXX_EXTENSIONS=ON
	-DCMAKE_EXPORT_COMPILE_COMMANDS=ON
	-DCMAKE_INCLUDE_CURRENT_DIR=ON
	-DCMAKE_BUILD_TYPE=$BUILD_TYPE
)

CMAKE_FLAGS_TARGET_SPECIFIC=(
    -DCMAKE_CXX_COMPILER='/usr/bin/g++'
    -DCMAKE_C_COMPILER='/usr/bin/gcc'

    -DCMAKE_CXX_FLAGS="-I $GTESTS_X86/include"
    -DCMAKE_LIBRARY_PATH="$GTESTS_X86/lib"
)

if [ "$QT_BUILD" = "ON" ]
then
	CMAKE_FLAGS_TARGET_SPECIFIC=(
		"${CMAKE_FLAGS_TARGET_SPECIFIC[@]}"
		-DCMAKE_AUTOMOC=ON
        -DCMAKE_AUTORCC=ON
		-DCMAKE_PREFIX_PATH=$QT_DIR_X86
	)
fi

mkdir -p $BUILD_DIR
cd $BUILD_DIR

set -x	# turn on mode of the shell where all executed commands are printed to the terminal.

$CMAKE "${CMAKE_FLAGS_COMMON[@]}" "${CMAKE_FLAGS_TARGET_SPECIFIC[@]}"
if [ $? -ne 0 ]	# check result of the previous command.
then
	echo -e "CMake deploy build env error"
	exit 20
fi

$CMAKE --build . --target ${TARGET} -- VERBOSE=1 -j ${CORES}
$CMAKE --build . --target install -- -j ${CORES}

set +x	# turn off mode of the shell where all executed commands are printed to the terminal.
exit 0