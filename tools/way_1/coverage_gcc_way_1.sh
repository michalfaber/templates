#!/bin/bash

SCRIPT_DIR=$( dirname $( realpath $0) )                                      # path to dir under which the currently executed script is located 
BUILD_DIR="$( realpath $SCRIPT_DIR/../../cpp/cmake/build_x86_all )"             # path to dir where a deploy of build was done 
COVERAGE_START_DIR="$( realpath $SCRIPT_DIR/../../cpp/cmake/cmake_way_1/src )"  # path to dir which is root dir to start generating coverage statistics 
COVERAGE_DIR=`pwd`/coverage                                                  # path to dir with a coverage output.

while [ $# -gt 0 ]
do
    argument="$1"
    case "$argument" in
        --build_dir=*)
            BUILD_DIR="${argument#*=}"
        ;;
        --src_dir=*)
            SRC_DIR="${argument#*=}"
        ;;
        --coverage_start_dir=*)
            COVERAGE_START_DIR="${1#*=}"
        ;;
        --coverage_dir=*)
            COVERAGE_DIR="${argument#*=}"
        ;;
        *)
            echo -e "Unknown argument: $argument!\n";
            exit 1
    esac

    shift
done

# Prepare for coverage by removing old coverage
find $BUILD_DIR -name "*.gcda" -print0 | while read -d $'\0' file
do
    rm "$file"
done
# Somehow it is generated during compilation. Getting rid of CMakeCXXCompilerId.gcno 
find $BUILD_DIR -name "CMakeCXXCompilerId.gcno" -print0 | while read -d $'\0' file
do
    rm "$file"
done

if [[ ! -d $COVERAGE_DIR ]]
then
    mkdir -p $COVERAGE_DIR
fi

# Create initial coverage. Files *.gcno are needed. Should write zero % code coverage.
lcov --no-external --capture --initial -b $COVERAGE_START_DIR --directory $BUILD_DIR --output-file "$COVERAGE_DIR/base.info"

# Generate files *.gcda related to coverage by executing tests.
cd $BUILD_DIR
make check_all

# Create coverage and generate html representation of the coverage.
lcov --no-external --capture -b $COVERAGE_START_DIR --directory $BUILD_DIR --output-file "$COVERAGE_DIR/test.info"
lcov --add-tracefile "$COVERAGE_DIR/base.info" --add-tracefile "$COVERAGE_DIR/test.info" --output-file "$COVERAGE_DIR/total.info"
genhtml "$COVERAGE_DIR/total.info" --output-directory "$COVERAGE_DIR/out"
