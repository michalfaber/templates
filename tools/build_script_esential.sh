# Example usage
#   ./build_script_simplified.sh --src_dir=/home/michal/projects/templates/cpp/cmake/cmake
#   ./build_script_simplified.sh --src_dir=/home/michal/projects/templates/cpp/printProgramName/printProgramName
#   ./build_script_simplified.sh --src_dir=/home/michal/projects/templates/cpp/appWithGui/appWithGui --qt_build=ON

COMPILER="GCC"
CXX_STANDARD=17
CORES=4
QT_BUILD=OFF

TARGET='all'
SRC_DIR=`pwd`
BUILD_TYPE='Release'
BUILD_BASE_PATH=`pwd`
INSTALL_DIR=`pwd`'/install'

CMAKE='/usr/bin/cmake'

PLATFORM='x86'
COMPILER_DIR_CLANG_X86='/usr'
COMPILER_DIR_GCC_X86='/usr'
QT_DIR_X86='/home/michal/tools/qtlibs/5.14.0/gcc_64'
GTESTS_X86='/home/michal/projects/qt/googletest/install/x86'

COMPILER_DIR_CLANG_EMB=''
COMPILER_DIR_GCC_EMB=''
OE_QMAKE_PATH_EXTERNAL_HOST_BINS=''
AARCH64_SYSROOT_DIR_EMB=''
QT_DIR_EMB="$AARCH64_SYSROOT_DIR_EMB/usr/"
GTESTS_EMB="$AARCH64_SYSROOT_DIR_EMB/usr/"

COVERAGE_FLAGS='-fprofile-arcs -ftest-coverage'
CMAKE_FLAGS_ADDITIONAL=()

# This below is the effective path which is set accordingly to chosen platform.
QT_DIR=""

show_usage_common()
{
	echo
	echo -e "Script arguments:";
	echo -e "--platform=     [ x86, emb ]       - set target platform for which the project must be built. Current: $PLATFORM";
	echo -e "--src_dir=      [<path>]           - set path to source directory. Current: $SRC_DIR";
	echo -e "--target=       [<name>]           - set name of target to be compiled. Current: $TARGET"
	echo -e "--install_dir=  [<path>]           - set path to instalation directory. If does not exist, it will be created. Current: $INSTALL_DIR";
	echo -e "--cmake=        [<path>/cmake]     - set path to cmake executable file. Current: $CMAKE";
	echo -e "--cxx_standard= [<number>]         - set c++ standard version to use. Current: $CXX_STANDARD";
	echo -e "--build_type=   [Release, Debug]   - turn off/on debug mode. Current: $BUILD_TYPE";
	echo -e "--cores=        [<number>]         - turn off/on debug mode. Current: $CORES";
	echo -e "--qt_build=     [ON, OFF]          - turn on/off build with qt libs. Current: $QT_BUILD";
	echo -e "--compiler=     [GCC, CLANG]       - choose compiler for building the project. Current: $COMPILER";
	echo -e "--cmake_flag=   [-D<name>=<value>] - add an additional cmake flag. Current: ${CMAKE_FLAGS_ADDITIONAL[@]}";
    echo -e "--coverage=     [ON, OFF]          - turn on/off build with code coverage. Current: ON";
}

while [ $# -gt 0 ]
do
    argument="$1"
	echo -e "\t Argument: $argument";
	case "$argument" in
		--platform=*)
			PLATFORM="${argument#*=}"
		;;
		--src_dir=*)
			SRC_DIR="${argument#*=}"
		;;
		--target=*)
			TARGET="${argument#*=}"
		;;
		--install_dir=*)
			INSTALL_DIR="${argument#*=}"
		;;
		--cmake=*)
			CMAKE="${argument#*=}"
		;;
		--cxx_standard=*)
			CXX_STANDARD="${argument#*=}"
		;;
		--debug=*)
			BUILD_TYPE="${argument#*=}"
		;;
		--cores=*)
			CORES="${argument#*=}"
		;;
		--qt_build=*)
			QT_BUILD="${argument#*=}"
		;;
		--compiler=*)
			COMPILER="${argument#*=}"
		;;
		--cmake_flag=*)
			CMAKE_FLAGS_ADDITIONAL=(
				"${CMAKE_FLAGS_ADDITIONAL[@]}"
				"${argument#*=}"
			)
		;;
        --coverage=*)
            if [ "${argument#*=}" = "OFF" ]
            then
                COVERAGE_FLAGS=''
            fi
		;;
		*)
			echo -e "Unknown argument: $argument!\n";
			show_usage_common
			exit 1
		;;
	esac
	shift
done

if [ "$PLATFORM" = "x86" ]
then
	QT_DIR=$QT_DIR_X86

	if [ "$COMPILER" = "CLANG" ]
	then
		CMAKE_FLAGS_TARGET_SPECIFIC=(
			-DCMAKE_CXX_COMPILER=$COMPILER_DIR_CLANG_X86/bin/clang++
			-DCMAKE_C_COMPILER=$COMPILER_DIR_CLANG_X86/bin/clang

			-DCMAKE_CXX_FLAGS="${COVERAGE_FLAGS} -I $GTESTS_X86/include -I ${COMPILER_DIR_GCC_X86}/include/c++/7.5.0 -L ${COMPILER_DIR_CLANG_X86}/lib"
			-DCMAKE_LIBRARY_PATH=$GTESTS_X86/lib
		)

	elif [ "$COMPILER" = "GCC" ]
	then
		CMAKE_FLAGS_TARGET_SPECIFIC=(
			-DCMAKE_CXX_COMPILER=$COMPILER_DIR_GCC_X86/bin/g++
			-DCMAKE_C_COMPILER=$COMPILER_DIR_GCC_X86/bin/gcc

			-DCMAKE_CXX_FLAGS="${COVERAGE_FLAGS} -I $GTESTS_X86/include -I ${COMPILER_DIR_GCC_X86}/include/c++/7.5.0 -L ${COMPILER_DIR_GCC_X86}/lib"
			-DCMAKE_LIBRARY_PATH=$GTESTS_X86/lib
		)
	fi
elif [ "$PLATFORM" = "emb" ]
then
	QT_DIR=$QT_DIR_EMB

	if [ "$COMPILER" = "CLANG" ]
	then
		CMAKE_FLAGS_TARGET_SPECIFIC=(
			-DCMAKE_CXX_COMPILER:STRING=$COMPILER_DIR_CLANG_EMB/aarch64-poky-linux-clang++
			-DCMAKE_C_COMPILER:STRING=$COMPILER_DIR_CLANG_EMB/aarch64-poky-linux-clang

			-DCMAKE_CXX_FLAGS:STRING="${COVERAGE_FLAGS} -I $AARCH64_SYSROOT_DIR_EMB/usr/include/c++/v1 -nostdinc++ --rtlib=compiler-rt --stdlib=libc++ -Qunused-arguments --sysroot=$AARCH64_SYSROOT_DIR_EMB -lc++fs -lrt -Wl,-z,notext"
			-DCMAKE_C_FLAGS:STRING="--rtlib=compiler-rt -Qunused-arguments -fPIC --sysroot=$AARCH64_SYSROOT_DIR_EMB"
			-DCMAKE_EXE_LINKER_FLAGS:STRING="-fuse-ld=$COMPILER_DIR_CLANG_EMB/aarch64-poky-linux-ld"
			-DCMAKE_SHARED_LINKER_FLAGS:STRING="-fuse-ld=$COMPILER_DIR_CLANG_EMB/aarch64-poky-linux-ld"
			-DOE_QMAKE_PATH_EXTERNAL_HOST_BINS=$OE_QMAKE_PATH_EXTERNAL_HOST_BINS
			-DCMAKE_LIBRARY_PATH=$GTESTS_EMB/lib
		)

	elif [ "$COMPILER" = "GCC" ]
	then
		CMAKE_FLAGS_TARGET_SPECIFIC=(
			-DCMAKE_CXX_COMPILER=$COMPILER_DIR_GCC_EMB/aarch64-poky-linux-g++
			-DCMAKE_C_COMPILER=$COMPILER_DIR_GCC_EMB/aarch64-poky-linux-gcc

			-DCMAKE_CXX_FLAGS="${COVERAGE_FLAGS} --sysroot=$AARCH64_SYSROOT_DIR_EMB"
			-DCMAKE_C_FLAGS="--sysroot=$AARCH64_SYSROOT_DIR_EMB"
			-DOE_QMAKE_PATH_EXTERNAL_HOST_BINS=$OE_QMAKE_PATH_EXTERNAL_HOST_BINS
			-DCMAKE_LIBRARY_PATH=$GTESTS_EMB/lib
		)
	fi
fi

if [ "$QT_BUILD" = "ON" ]
then
	CMAKE_FLAGS_TARGET_SPECIFIC=(
		"${CMAKE_FLAGS_TARGET_SPECIFIC[@]}"
		-DCMAKE_AUTOMOC=ON
        -DCMAKE_AUTORCC=ON
		-DCMAKE_PREFIX_PATH=$QT_DIR
	)
fi

CMAKE_FLAGS_COMMON=(
	$SRC_DIR
	-DCMAKE_INSTALL_PREFIX=$INSTALL_DIR/$PLATFORM
	-DCMAKE_CXX_STANDARD=$CXX_STANDARD
	-DCMAKE_CXX_STANDARD_REQUIRED=ON
	-DCMAKE_CXX_EXTENSIONS=ON
	-DCMAKE_EXPORT_COMPILE_COMMANDS=ON
	-DCMAKE_INCLUDE_CURRENT_DIR=ON
	-DCMAKE_BUILD_TYPE=$BUILD_TYPE
)

BUILD_TARGET_DIR="$BUILD_BASE_PATH/build_${PLATFORM}_${TARGET}"
mkdir -p $BUILD_TARGET_DIR
cd $BUILD_TARGET_DIR

set -x	# turn on mode of the shell where all executed commands are printed to the terminal.

$CMAKE "${CMAKE_FLAGS_COMMON[@]}" "${CMAKE_FLAGS_TARGET_SPECIFIC[@]}" "${CMAKE_FLAGS_ADDITIONAL[@]}"
if [ $? -ne 0 ]	# check result of the previous command.
then
	echo -e "CMake deploy build env error"
	exit 20
fi

$CMAKE --build . --target ${TARGET} -- VERBOSE=1 -j ${CORES}
$CMAKE --build . --target install -- -j ${CORES}

set +x	# turn off mode of the shell where all executed commands are printed to the terminal.
exit 0