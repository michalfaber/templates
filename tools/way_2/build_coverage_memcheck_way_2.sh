#!/bin/bash

SCRIPT_DIR=$( dirname $( realpath $0) )                                            # path to dir under which the currently executed script is located 
COMPILER='GCC'                                                                     # CLANG or GCC
SRC_DIR=$( realpath $SCRIPT_DIR/../../cpp/cmake/cmake_way_2 )                      # path to dir with CMakeList.txt
TEST_EXECUTABLE=$( realpath $SCRIPT_DIR/../../cpp/cmake/install/x86/bin/test_all ) # path to executable file with tests
BUILD_DIR="$( realpath $SCRIPT_DIR/../../cpp/cmake/build_x86_all )"                # path to dir where a deploy of build was done 
COVERAGE_START_DIR="$( realpath $SCRIPT_DIR/../../cpp/cmake/cmake_way_2/src )"     # path to dir which is root dir to start generating coverage statistics 

$SCRIPT_DIR/../build_script_esential.sh --src_dir=$SRC_DIR --compiler=$COMPILER

if   [ "CLANG" = "$COMPILER" ]
then
    $SCRIPT_DIR/coverage_clang_way_2.sh --coverage_start_dir=$COVERAGE_START_DIR --build_dir=$BUILD_DIR
elif [ "GCC" = "$COMPILER" ]
then
    $SCRIPT_DIR/coverage_gcc_way_2.sh --coverage_start_dir=$COVERAGE_START_DIR --build_dir=$BUILD_DIR
fi

valgrind --leak-check=full $TEST_EXECUTABLE